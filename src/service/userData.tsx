import HttpClient from "utils/api";

const TransferData = (idClient: string) => {
    return HttpClient.get(`Dados/DadosTransferencia?idCliente=${idClient}`)
}
const ContactData = (idClient: string) => {
    return HttpClient.get(`Dados/DadosContato?idCliente=${idClient}`)
}
const DocumentData = (idClient: string) => {
    return HttpClient.get(`File/GetFile?idCliente=${idClient}`)
}
const UserData = (idClient: string) => {
    return HttpClient.get(`Perfil/MeusDados?idCliente=${idClient}`)
}
export const UserDataService = {
    TransferData,
    ContactData,
    DocumentData,
    UserData
}