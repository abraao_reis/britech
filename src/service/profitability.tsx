import HttpClient from "utils/api";

const Profitability_Wallet = (walletId: string, date: string) => {
    return HttpClient.get(`Carteira/RentabilidadeCarteiraInicio?carteira=${walletId}&data=${date}&indice=1`)
}
const Profitability_Wallet_Month = (walletId: string, date: string) => {
    return HttpClient.get(`Carteira/RentabilidadeCarteiraMes?carteira=${walletId}&data=${date}&indice=1`)
}
const Profitability_Wallet_12Months = (walletId: string, date: string) => {
    return HttpClient.get(`Carteira/RentabilidadeCarteira12Meses?carteira=${walletId}&data=${date}&indice=1`)
}
const Profitability_Wallet_Year = (walletId: string, date: string) => {
    return HttpClient.get(`Carteira/RentabilidadeCarteiraAno?carteira=${walletId}&data=${date}&indice=1`)
}

export const ProfitabilityService = {
    Profitability_Wallet,
    Profitability_Wallet_Month,
    Profitability_Wallet_12Months,
    Profitability_Wallet_Year
}