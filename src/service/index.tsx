import { AuthService } from 'service/login';
import { DashboardService } from 'service/dashboard';
import { UserDataService } from 'service/userData';
import { WalletService } from 'service/wallet';
import { StatementService } from 'service/statement';
import { ProfitabilityService } from 'service/profitability';
import { FundsServices } from 'service/funds';

export {
    AuthService,
    DashboardService,
    UserDataService,
    WalletService,
    StatementService,
    ProfitabilityService,
    FundsServices
}