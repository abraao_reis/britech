import HttpClient from "utils/api";

const Statement = (idWallet: string, startDate: string, endDate: string) => {
    return HttpClient.get(`Extrato/ExtratoLancamentos?carteira=${idWallet}&dataInicio=${startDate}&dataFim=${endDate}`)
}
const Statement_In = (idWallet: string, startDate: string, endDate: string) => {
    return HttpClient.get(`Extrato/ExtratoLancamentosEntradas?carteira=${idWallet}&dataInicio=${startDate}&dataFim=${endDate}`);
}
const Statement_Out = (idWallet: string, startDate: string, endDate: string) => {
    return HttpClient.get(`Extrato/ExtratoLancamentosSaidas?carteira=${idWallet}&dataInicio=${startDate}&dataFim=${endDate}`);
}
const Statement_Future = (idWallet: string, startDate: string, endDate: string) => {
    return HttpClient.get(`Extrato/ExtratoLancamentosFuturos?carteira=${idWallet}&dataInicio=${startDate}&dataFim=${endDate}`);
}




export const StatementService = {
    Statement,
    Statement_Future,
    Statement_In,
    Statement_Out,
}