import HttpClient from "utils/api";

const Market_Heritage = (idWallet: string, date: string) => {
    return HttpClient.get(`Mercado/PatrimonioMercado?carteira=${idWallet}&data=${date}`)
}
const Market_ConsultationProfitability = (idWallet: string, date: string) => {
    return HttpClient.get(`Mercado/ConsultaRentabilidadeEstrategiaMercado?IdCarteira=${idWallet}&DataFim=${date}`);
}
const Market_Heritage_Grouped = (idWallet: string, date: string) => {
    return HttpClient.get(`Mercado/PatrimonioMercadoAgrupado?carteira=${idWallet}&data=${date}`);
}

export const WalletService = {
    Market_Heritage,
    Market_ConsultationProfitability,
    Market_Heritage_Grouped
}