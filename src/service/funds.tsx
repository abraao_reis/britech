import HttpClient from "utils/api";

const Funds_RFDetails = (walletId: string, date: string , cdAtivo:string | number) => {
    return HttpClient.get(`Mercado/DetalheRF?idCliente=${walletId}&data=${date}&idTitulo=${cdAtivo}`)
}
const Funds_Details = (walletId: string, date: string , cdAtivo:string | number) => {
    return HttpClient.get(`Mercado/DetalheFundos?idCliente=${walletId}&data=${date}&carteira=${cdAtivo}`)
}
const Funds_BMFDetails = (walletId: string, date: string ,  cdAtivo:string | number) => {
    return HttpClient.get(`Mercado/DetalheBMF?idCliente=${walletId}&data=${date}&ativo=${cdAtivo}`)
}
const Funds_StockExchange = (walletId: string, date: string , cdAtivo:string | number) => {
    return HttpClient.get(`Mercado/DetalheBolsa?idCliente=${walletId}&data=${date}&ativo=${cdAtivo}`)
}

const Funds_Total =  (walletId: string, date: string , mercado:string ) => {
    return HttpClient.get(`Mercado/TotalBrutoLiquido?idCliente=${walletId}&data=${date}&mercado=${mercado}`)
}

export const FundsServices = {
    Funds_Details,
    Funds_RFDetails,
    Funds_BMFDetails,
    Funds_StockExchange,
    Funds_Total
}