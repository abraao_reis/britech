import HttpClient from "utils/api.js";

const Login = (user: any) => {
    return HttpClient.post('Session/Create', user);
}
export const AuthService = {
    Login
}