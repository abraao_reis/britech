import HttpClient from "utils/api.js";

const Wallet_Heritage = (walletId: string, date: string) => {
    return HttpClient.get(`Carteira/PatrimonioCarteira?carteira=${walletId}&data=${date}`);
}
const Wallet_Market = (walletId: string, date: string) => {
    return HttpClient.get(`Carteira/PatrimonioMercado?carteira=${walletId}&data=${date}`);
}

export const DashboardService = {
    Wallet_Heritage,
    Wallet_Market
}