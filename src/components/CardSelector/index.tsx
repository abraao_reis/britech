import { IonCol, IonGrid, IonImg, IonRow } from "@ionic/react";
import { ButtonRightNarrow } from "assets/img";
import Card from "components/Card";
import { Body18, Subtitle12 } from "components/Typography";

interface CardSelectorProps extends React.HTMLAttributes<HTMLIonCardElement> {
    title?: string,
    subtitle?: string,
}
const CardSelector: React.FC<CardSelectorProps> = (props) => (
    <>
        <Card color='backgroundTertiary' {...props}>
            <IonGrid>
                <IonRow class="ion-align-items-center">
                    <IonCol size="10">
                        <Body18>{props.title}</Body18>
                        <Subtitle12>{props.subtitle}</Subtitle12>
                    </IonCol>
                    <IonCol size="2">
                        <IonImg src={ButtonRightNarrow} style={{ width: 40 }} />
                    </IonCol>
                </IonRow>
            </IonGrid>
        </Card>
    </>
)

export default CardSelector;