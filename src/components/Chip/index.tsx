import { IonChip } from '@ionic/react';
import styled from "styled-components";
const Chip = styled(IonChip)`
    background-color:${props => `var( --ion-color-${props.color})`};
`
export default Chip;