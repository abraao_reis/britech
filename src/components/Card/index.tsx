import { IonCard } from "@ionic/react";
import styled from "styled-components";
const Card = styled(IonCard)`
    border: 1px solid var(--ion-color-backgroundSecondary);
    box-sizing: border-box;
    border-radius: 8px;
    background-color: ${props => `var(--ion-color-${props.color})`};
    padding: 32px;
    margin: 26px 0px;
    box-shadow: none;
`

export default Card;