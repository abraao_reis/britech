import { IonCol, IonContent, IonGrid, IonRow, IonSpinner } from "@ionic/react";
import { Body18 } from "components/Typography";
import { useSelector } from "react-redux";
import { RootState } from "store";
const Loading: React.FC = () => {
    const { message } = useSelector((state: RootState) => state.GenericReducer);
    return (
        <IonContent>
            <IonGrid style={{ position: 'relative', top: '20%' }}>
                <IonRow className="ion-text-center">
                    <IonCol class="ion-align-self-center">
                        <Body18 className="ion-margin-vertical" color="tertiary">{message}</Body18>
                        <IonSpinner style={{ width: 120, height: 120, margin: 115 }} name="crescent" color="tertiary"></IonSpinner>
                    </IonCol>
                </IonRow>
            </IonGrid>
        </IonContent>
    )
}

export default Loading;