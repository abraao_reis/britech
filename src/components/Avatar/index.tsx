import { IonAvatar, IonGrid, IonImg, IonRow } from "@ionic/react";
import { Body16 } from "components/Typography";
import './styles.css';
interface AvatarProps extends React.HTMLAttributes<HTMLIonAvatarElement> {
    image?: any
    label?: string
}
const Avatar: React.FC<AvatarProps> = (props) => (
    <IonAvatar {...props} className="bgColorAvatar" color={props.image ? '' : 'tertiary'}>
        <IonGrid>
            <IonRow className="ion-justify-content-center ion-align-items-center">
                {props.image ? <IonImg src={props.image} /> : <Body16>{props.label}</Body16>}
            </IonRow>
        </IonGrid>
    </IonAvatar>
)

export default Avatar