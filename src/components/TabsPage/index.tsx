import { IonContent } from "@ionic/react"
import { TabsHeader } from "components"
import { TabsHeaderProps } from "components/TabsHeader"

const TabsPage: React.FC<TabsHeaderProps> = (props) => {
    return (
        <IonContent scrollEvents={true}>
            <TabsHeader avatar={props.avatar} subtitle={props.subtitle} component={props.component} height={props.height} paddingCard={props.paddingCard} />
            {props.children}
        </IonContent>

    )
}

export default TabsPage