import React from 'react';
import { IonImg } from '@ionic/react';
import './styles.css'
import {
    Eyesoff,
    Eyeson,
    AlertCircle,
    Check
} from 'assets/img'
import { Label12 } from 'components/Typography';

interface EyesButtonProps {
    onClick: () => {}
}
interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
    label?: string,
    error?: string,
    validated?: boolean,
    type: 'password' | 'number' | 'string' | 'text'
}
const Input: React.FC<InputProps> = (props) => {
    const [focus, setFocus] = React.useState(false);
    const [showPassword, setShowPassword] = React.useState(false);
    const ref = React.useRef<any>()

    return (
        <>
            <div className="label">
                <Label12>{props.label}</Label12>
            </div>
            <div
                className={`input-container ${focus ? 'focused' : 'unfocused'} ${props.validated ? 'validated' : 'error'} `}
                onClick={() => { setFocus(true) }}
            >
                <input
                    ref={ref}
                    className={`input-element ${focus ? 'focused' : 'unfocused'} `}
                    {...props}
                    type={props.type === 'password' ? showPassword === true ? 'text' : 'password' : props.type}
                />
                <div className="input-image">
                    {props.validated && <IonImg src={Check} />}
                    {props.error && <IonImg src={AlertCircle} />}
                    {
                        props.type === 'password' &&
                        <IonImg
                            onClick={() => {
                                setShowPassword(!showPassword)
                            }}
                            src={showPassword ? Eyeson : Eyesoff}
                        />
                    }
                </div>
            </div>
            {props.error && <span className='error'> {props.error}</span>}
        </>
    )
}

export default Input