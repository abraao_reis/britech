import { IonCol, IonGrid, IonImg, IonModal, IonRow } from "@ionic/react"
import { CloseModal } from 'assets/img';
import './styles.css'
const Modal: React.FC<React.HTMLAttributes<HTMLIonModalElement>> = (props) => {
    return (
        <>
            <IonModal {...props}>
                <IonGrid style={{ width: '100%' }}>
                    <IonRow>
                        <IonCol>
                            {props.children}
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol onClick={props.onClick}>
                            <IonImg src={CloseModal} className='close-modal' />
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonModal>
        </>
    )
}

export default Modal