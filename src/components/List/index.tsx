import { IonCol, IonGrid, IonImg, IonItem, IonList, IonRow } from "@ionic/react"
import {Heading16, Subtitle12, Body16, Body14 } from 'components/Typography';
import { ArrowDown, ArrowUp, ChevronRight } from "assets/img";
import { IStatement, IStatementList } from "interface/statement";
import { dateFormatPtBR } from "utils/miscelaneous";

interface IListItem {
    title: string;
    subtitle?: string;
    value?: string
}
interface IListProps {
    items: IListItem[];
    onClick: (e:IListItem)=>void;
}
const ListArrow: React.FC<IListProps> = (props) => {
    return (
        <>
            <IonList>
                {props.items.map((item: IListItem, index: number) => (
                    <IonItem key={index}>
                        <IonGrid>
                            <IonRow onClick={()=>{ props.onClick(item) }}>
                                <IonCol size="11">
                                    <Heading16>{item.title}</Heading16>
                                    { item.subtitle && 
                                        <Subtitle12>{item.subtitle}</Subtitle12>
                                    }
                                </IonCol>
                                <IonCol size="1">
                                    <IonImg src={ChevronRight} />
                                </IonCol>
                            </IonRow>

                        </IonGrid>
                    </IonItem>
                ))}

            </IonList>
        </>
    )
}
const ListStatement: React.FC<IStatementList> = (props) => {
    return (
        <>
            <IonList>
                {props.items.map((item: IStatement, index: number) => (
                    <IonItem key={index}>
                        <IonGrid>
                            <IonRow 
                                className="
                                    ion-justify-content-between
                                    ion-align-items-center
                                ">
                                <IonCol size="7">
                                    <Body14>{item.descricao}</Body14>
                                    <Subtitle12>{dateFormatPtBR(item.dataVencimento)}</Subtitle12>
                                </IonCol>
                                <IonCol size="5">
                                    <IonGrid>
                                        <IonRow class="ion-align-items-center">
                                            <IonCol size="2">
                                                <IonImg src={item.valor > 0 ? ArrowUp : ArrowDown} />
                                            </IonCol>
                                            <IonCol size="10">
                                                <Body14 color={item.valor > 0 ? "success" : "danger"}>
                                                    {`${Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
                                                    .format(Math.abs(item.valor))}`}
                                                </Body14>
                                            </IonCol>
                                        </IonRow>
                                    </IonGrid>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonItem>
                ))}

            </IonList>
        </>
    )
}
export default {
    ListStatement,
    ListArrow,
}