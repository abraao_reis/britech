import { IonCol, IonGrid, IonRow } from '@ionic/react';
import { BackButton } from 'components';
import { Body18 } from 'components/Typography';
import './styles.css'

interface IHeaderProps {
    title?: string;
    fill?: boolean;
}
const Header: React.FC<IHeaderProps> = (props) => (
    <IonGrid style={{ padding: 0 }}>
        <IonRow className={props.fill ? 'headerPrimary' : 'headerSecondary'}>
            <IonCol size='2'>
                <BackButton className="imageHeader" />
            </IonCol>
            <IonCol size='8' class="ion-align-self-center">
                <Body18 className='ion-text-center' color={props.fill ? 'light' : 'primary'}>{props.title}</Body18>
            </IonCol>
        </IonRow>
        <IonRow>
            {props.children}
        </IonRow>
    </IonGrid >
)

export default Header;