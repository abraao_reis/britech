import { IonImg } from "@ionic/react"
import { ChevronLeft } from 'assets/img';
import { useHistory } from 'react-router-dom';
const BackButton: React.FC<React.HTMLAttributes<HTMLIonImgElement>> = (props) => {
    const history = useHistory();
    return (
        <>
            <IonImg {...props} style={{ width: 40 }} onClick={() => { history.goBack() }} src={ChevronLeft} />
        </>
    )
}
export default BackButton;