import styled from 'styled-components';

interface TypographyProps {
    lock?:boolean;
    color?:string;
}
export const Typography = styled.p<TypographyProps>`
    margin:5px 0px;
    color:${props => props.lock ? 'var( --ion-color-backgroundPrimary)!important' : 'var(--ion-color-primary)'};
    background-color:${props => props.lock ? 'var( --ion-color-backgroundPrimary)' : 'none' };
    border-radius: ${props => props.lock ? '8px' : '0px'};
`
export const Heading32 = styled(Typography)`
/* App/Headline/headline-32B */
font-family: 'Inter';
font-style: normal;
font-weight: bold;
font-size: 32px;
line-height: 118%;
letter-spacing: 0.0025em;
color:${props => `var( --ion-color-${props.color})`};

`
export const Heading28 = styled(Typography)`
font-family: Inter;
font-style: normal;
font-weight: bold;
font-size: 28px;
line-height: 140%;
color:${props => `var( --ion-color-${props.color})`};
`

export const Heading24 = styled(Typography)`
    /* App/Headline/headline-24B */

font-family: 'Inter';
font-style: normal;
font-weight: bold;
font-size: 24px;
line-height: 130%;
color:${props => `var( --ion-color-${props.color})`};
`
export const Heading16 = styled(Typography)`
font-family: 'Inter';
font-style: normal;
font-weight: 500;
font-size: 18px;
line-height: 150%;
letter-spacing: -0.01em;
color:${props => `var( --ion-color-${props.color})`};
`
export const Body18 = styled(Typography)`
    font-family: 'Inter';
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    line-height: 150%;
    color:${props => `var( --ion-color-${props.color})`};
`
export const Body16 = styled(Typography)`
    font-family: 'Inter';
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 150%;
    letter-spacing: -0.005em;
    color:${props => `var( --ion-color-${props.color})`};
`
export const Body14 = styled(Typography)`
    font-family: 'Inter';
    font-style: normal;
    font-weight: 800;
    font-size: 14px;
    line-height: 150%;
    letter-spacing: -0.005em;
    color:${props => `var( --ion-color-${props.color})`};
`
export const Subtitle12 = styled(Typography)`
font-family: 'Inter';
font-style: normal;
font-weight: normal;
font-size: 12px;
line-height: 15px;
color:${props => `var( --ion-color-${props.color})`};

`
export const Label12 = styled(Typography)`
    font-family: 'Inter';
    font-style: normal;
    font-weight: bold;
    font-size: 12px;
    line-height: 150%;
    text-transform: uppercase;
    color:${props => `var( --ion-color-${props.color})`}
`