import { List, Modal } from "components";
import { IonCol, IonImg, IonRow } from '@ionic/react';
import { ChevronDown } from 'assets/img';
import { useState } from "react";
import { ISelect } from 'interface/select';
import { Heading16, Subtitle12 } from "components/Typography";
import './styles.css';
interface ISelectProps {
    title:string,
    menuTitle?:string;
    items:ISelect[],
    onChangeSelect:(v:any)=>void
}
const Select:React.FC<ISelectProps> = (props) => {
const [modal,setModal] = useState(false);
    return (
        <>
                <IonRow onClick={()=>{setModal(true)}} 
                    className="
                        ion-align-items-center 
                        ion-justify-content-center
                        select
                    "
                >
                    <IonCol size="6">
                        <Subtitle12>
                            { props.title } 
                        </Subtitle12>
                    </IonCol>
                    <IonCol size="3">
                        <IonImg src={ChevronDown} />
                    </IonCol>
                </IonRow>
            <IonRow>
                <IonCol>
                    <Modal
                        isOpen={modal}
                        breakpoints={[0.1, 0.5, 1]}
                        initialBreakpoint={0.6}
                        onClick={() => { setModal(false) } }
                        backdropDismiss={false}
                        showBackdrop={true}
                    >
                        <IonRow>
                            <IonCol>
                                <Heading16 color="primary">{props.menuTitle}</Heading16>
                            </IonCol>
                        </IonRow>
                        <IonRow>
                            <IonCol>
                                <List.ListArrow
                                items={props.items} 
                                onClick={
                                    (value)=>{
                                        props.onChangeSelect(value);
                                        setModal(false);
                                }}
                            />
                            </IonCol>
                        </IonRow>
                    </Modal>
                </IonCol>
            </IonRow>
        </>
    )
}

export default Select;