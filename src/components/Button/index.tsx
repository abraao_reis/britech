import { IonButton } from "@ionic/react";
import styled from "styled-components";

const Button = styled(IonButton)`
    height:40px;
    font-family: 'Inter';
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 16px;
    text-align: center;
    color: #FFFFFF;
`

export default Button;