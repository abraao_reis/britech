import { IonCol, IonGrid, IonIcon, IonImg, IonRow } from "@ionic/react";
import Card from "components/Card";
import { Body18 } from "components/Typography";
import React from "react";
import {
    Eyesoff,
    Eyeson,
    Notification
} from 'assets/img'
import { Avatar, BackButton } from "components";
import './styles.css'
import { useSelector } from 'react-redux';
import { RootState } from "store";
import { useHistory } from 'react-router';
import { useDispatch } from 'react-redux';
import { setLock } from "store/reducers/generic";

export interface TabsHeaderProps {
    avatar?: boolean;
    subtitle?: string;
    component?: React.FC | null;
    height?:string | number;
    paddingCard?:string;
}
const TabsHeader: React.FC<TabsHeaderProps> = (props) => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { lock } = useSelector((state: RootState) => state.GenericReducer);
    const { nome } = useSelector((state: RootState) => state.UserReducer.user);
    return (
        <>
            <Card className='tabsHeader' style={{height:`${props.height ? props.height : '210px'}`}} color='primary'>
                <IonGrid>
                    <IonRow class="ion-justify-content-start ion-align-items-center">
                        <IonCol size="2">
                            {props.avatar ?
                                <Avatar onClick={()=>{history.push('/home/perfil')}} label={nome.substring(0, 1).toUpperCase()}/>
                                :
                                <BackButton className="imageHeader" />
                            }
                        </IonCol>
                        <IonCol size="8">
                            <Body18 className="ion-text-center" color="light">{props.subtitle}</Body18>
                        </IonCol>
                        <IonCol size="1">
                            <IonImg
                                className="imageHeader"
                                onClick={() => {
                                    dispatch(
                                        setLock(!lock)
                                    );
                                }}
                                src={lock ? Eyesoff : Eyeson}
                            />
                        </IonCol>
                        <IonCol size="1">
                            <IonImg src={Notification} onClick={() => { history.push('/dashboard/notification') }} />
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </Card>
            {props.component &&
                <Card color="light" style={{padding:`${props.paddingCard ? props.paddingCard : '32px'}`}} className="cardFloating">
                    {props.component}
                </Card>
            }
        </>
    )
}
export default TabsHeader