import { IonGrid, IonRow, IonCol } from "@ionic/react";
import { Heading16, Subtitle12, Body16 } from "components/Typography";
import React from "react";
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from "store";
import { getTransferencia } from "store/middleware";

const Transfer: React.FC = () => {
    const dispatch = useDispatch();
    const { transferencia } = useSelector((state: RootState) => state.UserDataReducer)
    React.useEffect(() => {
        dispatch(getTransferencia());
    }, [dispatch])

    return (

        <>
            <IonGrid>
                <IonRow>
                    <IonCol>
                        <Heading16 color="primary">Transferências</Heading16>
                        <Subtitle12 color="gray04">Para novas aplicações, efetue uma transferência para
                            a conta abaixo, indentificando seu CPF.</Subtitle12>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        <Subtitle12 color="gray05">Banco</Subtitle12>
                        <Body16 color="primary">{transferencia.banco}</Body16>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        <Subtitle12 color="gray05">Conta corrente</Subtitle12>
                        <Body16 color="primary">{transferencia.cc}</Body16>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        <Subtitle12 color="gray05">Tipo de transferência</Subtitle12>
                        <Body16 color="primary">{transferencia.tipoTransferencia}</Body16>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        <Subtitle12 color="gray05">Finalidade</Subtitle12>
                        <Body16 color="primary">{transferencia.finalidade}</Body16>
                    </IonCol>
                </IonRow>
            </IonGrid>
        </>
    )
}

export default Transfer;