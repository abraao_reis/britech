import { IonCol, IonGrid, IonImg, IonRow } from "@ionic/react";
import { Call, Folder, Transferencia } from "assets/img";
import { Modal, TabsPage } from "components";
import { Card } from "components";
import { Body14, Subtitle12 } from "components/Typography";
import DashboardHeader from 'pages/Dashboard/DashboardHeader'
import React from "react";
import './styles.css'
import Contact from './Contact';
import Transfer from "./Transfer";
import { useDispatch } from 'react-redux';
import { getCarteiraPatrimonio, getCarteiraMercado } from 'store/middleware/dashboard';
import { useSelector } from 'react-redux';
import { RootState } from "store";
import { useHistory } from "react-router";
import moment from "moment";

const Dashboard: React.FC = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const { patrimonio } = useSelector((state: RootState) => state.DashReducer);
    const { dataCliente } = useSelector((state: RootState) => state.UserReducer.user);
    const { lock } = useSelector((state:RootState)=> state.GenericReducer);
    const [modal, setModal] = React.useState({
        openModal: false,
        component: ''
    });

    React.useEffect(() => {
        dispatch(getCarteiraPatrimonio())
        dispatch(getCarteiraMercado())
    }, [dispatch])
    const handleModalInfo = (componentName: string) => {
        if (componentName === 'contato') {
            setModal({ openModal: true, component: 'contato' })
        } else {
            setModal({ openModal: true, component: 'transferencia' })
        }
    }
    return (
        <TabsPage paddingCard="8px" avatar={true} subtitle="Dashboard" isTabCard={true} component={<DashboardHeader lock={lock} patrimonioCarteira={patrimonio.patrimonioCarteira} />}>
            <IonGrid>
                <IonRow className="ion-justify-content-center">
                    <IonCol>
                        <Body14>Posição consolidada</Body14>
                        <Subtitle12>{`Dados consolidados em ${moment(dataCliente).format("DD/MM/yyyy")}`}</Subtitle12>
                    </IonCol>
                </IonRow>
                <IonRow className="ion-justify-content-around">
                    <IonCol size="6">
                        <Card color="backgroundTertiary">
                            <Body14 lock={lock}>{`${Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
                            .format(Number(patrimonio.patrimonioTotal))}`}</Body14>
                            <Subtitle12>Patrimônio total</Subtitle12>
                        </Card>

                    </IonCol>
                    <IonCol size="6">
                        <Card color="backgroundTertiary">
                            <Body14 lock={lock}>{`${Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
                            .format(Number(patrimonio.contaCorrente))}`}</Body14>
                            <Subtitle12>Saldo em conta</Subtitle12>

                        </Card>

                    </IonCol>
                </IonRow>
                <IonRow className="ion-justify-content-around">
                    <IonCol size="3" onClick={() => { handleModalInfo('transferencia') }}>
                        <Card color="backgroundTertiary">
                            <IonImg src={Transferencia} />
                        </Card>
                        <Body14 className="textAlignCenter">Transferência</Body14>
                    </IonCol>
                    <IonCol size="3" onClick={() => { handleModalInfo('contato') }}>
                        <Card color="backgroundTertiary">
                            <IonImg src={Call} />
                        </Card>
                        <Body14 className="textAlignCenter">Contato</Body14>
                    </IonCol>
                    <IonCol size="3" onClick={() => { history.push('/home/documentos') }}>
                        <Card color="backgroundTertiary">
                            <IonImg src={Folder} />
                        </Card>
                        <Body14 className="textAlignCenter">Documentos</Body14>
                    </IonCol>
                </IonRow>
            </IonGrid>
            <Modal
                isOpen={modal.openModal}
                breakpoints={[0.1, 0.5, 1]}
                initialBreakpoint={0.5}
                backdropDismiss={false}
                showBackdrop={true}
                onClick={() => { setModal({ openModal: false, component: '' }) }}
            >
                {modal.component === 'contato' ? <Contact /> : <Transfer />}
            </Modal>
        </TabsPage>
    )
}

export default Dashboard;
