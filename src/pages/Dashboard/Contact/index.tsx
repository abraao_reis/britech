import { IonGrid, IonRow, IonCol } from "@ionic/react";
import { Body16, Heading16, Subtitle12 } from "components/Typography";
import React from "react";
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from "store";
import { getContato } from 'store/middleware/userData';
const Contact: React.FC = () => {
    const { contato } = useSelector((state: RootState) => state.UserDataReducer)
    const dispatch = useDispatch();

    React.useEffect(() => {
        dispatch(getContato());
    },[dispatch])

    return (
        <>
            <IonGrid>
                <IonRow>
                    <IonCol>
                        <Heading16 color="primary">Contato</Heading16>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        <Subtitle12 color="gray05">Nome</Subtitle12>
                        <Body16 color="primary">{contato.nome ?? 'Não definido'}</Body16>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        <Subtitle12 color="gray05">Telefone</Subtitle12>
                        <Body16 color="primary">{contato.telefone}</Body16>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        <Subtitle12 color="gray05">E-mail</Subtitle12>
                        <Body16 color="primary">{contato.email}</Body16>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        <Subtitle12 color="gray05">Endereço</Subtitle12>
                        <Body16 color="primary">{contato.endereco ?? 'Não definido'}</Body16>
                    </IonCol>
                </IonRow>
            </IonGrid>
        </>
    )
}


export default Contact;