import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Pie } from 'react-chartjs-2';

ChartJS.register(ArcElement);

interface IDashboardChart {
    data:any
}
const DashboardChart:React.FC<IDashboardChart> = (props) => {
    return(
        <Pie data={props?.data} />
    )
}

export default DashboardChart;