import { IonCol, IonContent, IonGrid, IonImg, IonPage, IonRow } from "@ionic/react"
import { Header } from "components"
import React from "react"
import { useDispatch } from 'react-redux';
import { getDocument } from "store/middleware";
import { useSelector } from 'react-redux';
import { RootState } from "store";
import { IDocument } from "interface/userData";
import { Body16, Subtitle12 } from "components/Typography";
import { Download } from 'assets/img';

const DocumentsPage: React.FC = () => {
    const { document } = useSelector((state: RootState) => state.UserDataReducer)
    const dispatch = useDispatch()
    React.useEffect(() => {
        dispatch(getDocument());
    }, [dispatch])
    const handleDownloadDocuments = (doc:IDocument) => {
        const arr = doc.fileBytes;
        const byteArray = new Uint8Array(arr);
        const link = window.document.createElement('a');
        link.href = window.URL.createObjectURL(new Blob([byteArray], { type: `application/${doc.extensao}` }));
        link.download = `${doc.nameFile}.${doc.extensao}`; 
        window.document.body.appendChild(link);
        link.click();
        window.document.body.removeChild(link);
    }
    return (
            <IonContent>
                <Header title="Documentos" fill={true}>
                    <IonGrid>
                        {document.map((doc: IDocument, index: number) => (
                            <IonRow key={index}>
                                <IonCol size="10">
                                    <Body16 color="primary">{doc.nameFile ?? 'Não definido'}</Body16>
                                    <Subtitle12>{doc.date}</Subtitle12>
                                </IonCol>
                                <IonCol size="2">
                                    <IonImg src={Download} onClick={()=>{handleDownloadDocuments(doc)}}/>
                                </IonCol>
                            </IonRow>
                        ))}
                    </IonGrid>
                </Header>
            </IonContent>
    )
}
export default DocumentsPage