import { IonChip, IonCol, IonGrid, IonLabel, IonRow, IonText } from '@ionic/react';
import { Heading16, Subtitle12 } from 'components/Typography';
import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from 'store';
import DashboardChart from 'pages/Dashboard/DashboardChart';
interface IDashboardHeaderProps {
    patrimonioCarteira: string,
    lock:boolean
}
const DashboardHeader: React.FC<IDashboardHeaderProps> = (props) => {
    const { mercado } = useSelector((state: RootState) => state.DashReducer)

    return (
        <IonGrid>
            <IonRow className="ion-justify-content-between ion-align-items-center">
                <IonCol size="5">
                    <DashboardChart data={JSON.parse(JSON.stringify(mercado))} />
                </IonCol>
                <IonCol size="7">
                    <Heading16 color='primary' lock={props.lock}>
                        {`${Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
                            .format(Number(props.patrimonioCarteira))}`}
                    </Heading16>
                    <Subtitle12 color='primary'>
                        Carteira de ativos
                    </Subtitle12>
                </IonCol>
            </IonRow>
            <IonRow>
                {mercado.datasets[0].legends.map((value: any, index: number) => (
                    <IonChip key={index}>
                        <IonText style={{
                            color: mercado.datasets[0].legendsColors[index],
                            fontSize: '75px',
                            alignSelf: 'end',
                            margin: '-6px auto'
                        }}>.</IonText>
                        <IonLabel color="dark">{value}</IonLabel>
                    </IonChip>
                ))}
            </IonRow>
        </IonGrid>
    )
}

export default DashboardHeader;
