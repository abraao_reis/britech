import Login from 'pages/Login';
import AccountSelector from 'pages/AccountSelector';
import Dashboard from 'pages/Dashboard';
import WalletPage from 'pages/Wallet';
import StatementPage from 'pages/Statement';
import EvolutionPage from 'pages/Evolution';
import DocumentPage from 'pages/Dashboard/Documents';
import NotificationPage from 'pages/Notification';
import MyDataPage from 'pages/MyData';
import ProfilePage from 'pages/Profile';
import FundsPage from 'pages/Funds';

export {
    Login,
    AccountSelector,
    Dashboard,
    WalletPage,
    StatementPage,
    EvolutionPage,
    DocumentPage,
    NotificationPage,
    MyDataPage,
    ProfilePage,
    FundsPage,
}