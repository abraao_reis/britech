import { IonList, IonItem, IonGrid, IonRow, IonCol, IonImg } from "@ionic/react"
import { ArrowUp, ArrowDown } from "assets/img"
import { Body14, Subtitle12 } from "components/Typography"
import { IStatement, IStatementList } from "interface/statement"
import { dateFormatPtBR } from "utils/miscelaneous"

const StatementList: React.FC<IStatementList> = (props) => {
    return (
        <>
            <IonList>
                {props.items.map((item: IStatement, index: number) => (
                    <IonItem key={index}>
                        <IonGrid>
                            <IonRow 
                                className="
                                    ion-justify-content-between
                                    ion-align-items-center
                                ">
                                <IonCol size="7">
                                    <Body14>{item.descricao}</Body14>
                                    <Subtitle12>{dateFormatPtBR(item.dataVencimento)}</Subtitle12>
                                </IonCol>
                                <IonCol size="5">
                                    <IonGrid>
                                        <IonRow class="ion-align-items-center">
                                            <IonCol size="2">
                                                <IonImg src={item.valor > 0 ? ArrowUp : ArrowDown} />
                                            </IonCol>
                                            <IonCol size="10">
                                                <Body14 lock={props.lock} color={item.valor > 0 ? "success" : "danger"}>
                                                    {`${Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
                                                    .format(Math.abs(item.valor))}`}
                                                </Body14>
                                            </IonCol>
                                        </IonRow>
                                    </IonGrid>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonItem>
                ))}

            </IonList>
        </>
    )
}
export default StatementList;