import { Chip, Select, TabsPage } from "components"
import { Body14, Subtitle12 } from '../../components/Typography/index';
import { IonCol, IonGrid, IonRow } from "@ionic/react";
import { useDispatch, useSelector } from 'react-redux';
import { getStatementAll, getStatementFuture, getStatementIn, getStatementOut } from "store/middleware";
import { RootState } from "store";
import React from "react";
import { ShortPeriod } from "mock/select";
import './style.css'
import StatementHeader from "./StatementHeader";
import StatementList from "./StatementList";
const Statement: React.FC = () => {
    const dispatch = useDispatch();
    const { patrimonio } = useSelector((state: RootState) => state.DashReducer);
    const { statement } = useSelector((state:RootState)=>state.StatementReducer)
    const [period , setPeriod] = React.useState(30);
    const [statementModality, setStatementModality] = React.useState('all');
    const { lock } = useSelector((state:RootState)=> state.GenericReducer);
    React.useEffect(()=>{
        dispatch(getStatementAll(30));
    },[dispatch]);
    
    const handleStatementChange = (modality:string , period:number) => {
        switch(modality){
            case 'all' :
                dispatch(getStatementAll(period)); 
            break;
            case 'in' : 
                dispatch(getStatementIn(period));
            break;
            case 'out' : 
                dispatch(getStatementOut(period));
            break;
            case 'future' : 
                dispatch(getStatementFuture(period));
            break;
        }
    }
    return (
        <TabsPage  avatar={false} component={
            <StatementHeader lock={lock} patrimonio={patrimonio.patrimonioCarteira} 
            />} 
            subtitle="Extrato">
            <IonGrid>
                <IonRow className="textAlignCenter">
                    <IonCol className="on-text-center">
                        <Chip color={statementModality === 'all' ? 'backgroundPrimary' : 'backgroundTertiary'}  
                        onClick={()=>{
                            dispatch(getStatementAll(period));
                            setStatementModality('all');
                        }}>
                            <Subtitle12>Todas</Subtitle12>
                        </Chip>
                    </IonCol>
                    <IonCol className="on-text-center">
                        <Chip color={statementModality === 'in' ? 'backgroundPrimary' : 'backgroundTertiary'}
                        onClick={()=> { 
                            dispatch(getStatementIn(period));
                            setStatementModality('in');
                        }}>
                            <Subtitle12>Entradas</Subtitle12>
                        </Chip>
                    </IonCol>
                    <IonCol className="on-text-center">
                        <Chip color={statementModality === 'out' ? 'backgroundPrimary' : 'backgroundTertiary'}
                        onClick={()=>{
                            dispatch(getStatementOut(period));
                            setStatementModality('out');
                        }}>
                            <Subtitle12>Saidas</Subtitle12>
                        </Chip>
                    </IonCol>
                    <IonCol className="on-text-center">
                        <Chip color={statementModality === 'future' ? 'backgroundPrimary' : 'backgroundTertiary'}
                        
                        onClick={()=>{
                            dispatch(getStatementFuture(period));
                            setStatementModality('future');
                        }}>
                            <Subtitle12>Futuro</Subtitle12>
                        </Chip>
                    </IonCol>
                </IonRow>
                <IonRow className="
                    ion-justify-content-between
                    ion-align-items-center
                    filter
                ">
                    <IonCol size="4">
                        <Body14 className="on-text-center">Periodo</Body14>
                    </IonCol>
                    <IonCol size="4">
                        <Select items={ShortPeriod}
                            title={`${period} dias`}
                            menuTitle="Selecione um período"
                            onChangeSelect={(item)=>{
                                setPeriod(item.value);
                                handleStatementChange(statementModality,item.value);
                                
                            }}
                        />
                    </IonCol>
                </IonRow>
                </IonGrid>
                <IonGrid>
                <IonRow>
                    <IonCol>
                        <StatementList lock={lock} items={
                            statement
                        }
                        />
                    </IonCol>
                </IonRow>
            </IonGrid>
        </TabsPage>
    )
}

export default Statement