import { IonGrid, IonRow, IonCol, IonImg } from "@ionic/react";
import { MoneyCard } from "assets/img";
import { Heading24, Subtitle12 } from "components/Typography";
import { lookupService } from "dns";

interface StatementHeaderProps {
    patrimonio: string,
    lock:boolean,
}
const StatementHeader: React.FC<StatementHeaderProps> = (props) => {
    return (
        <IonGrid style={{marginTop:10,}}>
            <IonRow className="ion-justify-content-between ion-align-items-center">
                <IonCol size="2">
                    <IonImg src={MoneyCard} />
                </IonCol>
                <IonCol size="10">
                    <Heading24 lock={props.lock}>
                        {`
                            ${Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
                            .format(Number(props.patrimonio))}
                        `}
                    </Heading24>
                    <Subtitle12>Saldo Disponível</Subtitle12>
                </IonCol>
            </IonRow>
        </IonGrid>
    )
}

export default StatementHeader;