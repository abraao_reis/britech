import { IonContent, IonPage, IonText, IonGrid, IonRow, IonCol, IonImg } from '@ionic/react';
import { Input, Button, Container } from 'components';
import { useHistory } from "react-router-dom";
import './styles.css'
import Logo from 'assets/logo/logo.png';
import React, { useEffect } from 'react';
import { Body16, Heading24 } from 'components/Typography';
import { postLogin } from 'store/middleware/login';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'store';

const Login: React.FC = () => {
    const dispatch = useDispatch();
    const { isLoggedIn } = useSelector((state: RootState) => state.AuthReducer)
    const [login, setLogin] = React.useState('');
    const [password, setPassword] = React.useState('');
    const history = useHistory();

    useEffect(() => {
        if (isLoggedIn) { history.push('/accountSelector') }
    }, [history, isLoggedIn]);
    const onSubmit = () => {
        dispatch(
            postLogin({ login, password })
        )
    }
    return (
        <IonPage>
            <IonContent scrollEvents={true}>
                <Container>
                    <IonGrid>
                        <IonRow style={{ marginTop: 60 }}>
                            <IonCol size='4'>
                                <IonImg src={Logo} />
                            </IonCol>
                        </IonRow>
                        {/* <IonRow style={{ marginTop: 64 }}>
                            <Heading24 color="primary">
                                Bem vindo ao Smart SAT!
                            </Heading24>
                        </IonRow> */}
                        <IonRow>
                            <Body16>
                                Acesse sua conta
                            </Body16>
                        </IonRow>
                        <IonRow style={{ marginTop: 48 }}>
                            <Input type='string' value={login} onChange={(e) => { setLogin(e.target.value) }} maxLength={50} label="Login" placeholder="Login" />
                        </IonRow>
                        <IonRow>
                            <Input type='password' onChange={(e) => { setPassword(e.target.value) }} value={password} maxLength={50} label="Senha" placeholder="Senha" />
                        </IonRow>
                        <IonRow style={{ marginTop: 52 }}>
                            <IonCol size="12">
                                <Button onClick={onSubmit} expand="full" shape="round">
                                    <IonText color="light">
                                        Entrar
                                    </IonText>
                                </Button>
                            </IonCol>
                        </IonRow>
                    </IonGrid>
                </Container>
            </IonContent>
        </IonPage>
    )
}

export default Login;