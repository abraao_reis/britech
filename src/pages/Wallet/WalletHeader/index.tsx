import { IonGrid, IonRow, IonCol, IonImg } from "@ionic/react";
import { WalletPurple } from "assets/img";
import { Heading24, Subtitle12 } from "components/Typography";

interface WalletHeaderProps {
    patrimonio: string,
    lock:boolean,
}
const WalletHeader:React.FC<WalletHeaderProps> = (props) => {
    return (
        <IonGrid style={{marginTop:10,}}>
            <IonRow className="ion-justify-content-between ion-align-items-center">
                <IonCol size="2">
                    <IonImg src={WalletPurple} />
                </IonCol>
                <IonCol size="10">
                    <Heading24 lock={props.lock}>
                        {`
                            ${Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
                            .format(Number(props.patrimonio))}
                        `}
                    </Heading24>
                    <Subtitle12>Patrimônio investido</Subtitle12>
                </IonCol>
            </IonRow>
        </IonGrid>
    )
}

export default WalletHeader;