import { IonGrid, IonRow, IonCol } from "@ionic/react";
import { TabsPage } from "components";
import { Body14, Subtitle12 } from "components/Typography";
import { useDispatch } from 'react-redux';
import React from "react";
import { getMercadoPatrimonioCarteira, getMercadoRentabilidadeCarteira, getMarketHeritageGrouped } from 'store/middleware';
import { useSelector } from 'react-redux';
import { RootState } from "store";
import { ListArrowChart } from "./ListArrowWallet";
import moment from "moment";
import WalletHeader from "./WalletHeader/index";

const Wallet: React.FC = () => {
    const dispatch = useDispatch()
    const { patrimonio } = useSelector((state: RootState) => state.DashReducer)
    const { marketHeritageGrouped } = useSelector((state:RootState) => state.MarketReducer)
    const { dataCliente } = useSelector((state: RootState) => state.UserReducer.user);
    const { lock } = useSelector((state:RootState)=> state.GenericReducer);

    React.useEffect(() => {
        dispatch(getMercadoPatrimonioCarteira());
        dispatch(getMercadoRentabilidadeCarteira());
        dispatch(getMarketHeritageGrouped());
    }, [dispatch])

    return (
        <TabsPage avatar={false} subtitle="Carteira" isTabCard={true} component={<WalletHeader lock={lock} patrimonio={patrimonio.patrimonioCarteira} />}>
            <IonGrid>
                <IonRow className="ion-justify-content-center">
                    <IonCol>
                        <Body14>Consolidada por estratégia</Body14>
                        <Subtitle12>{`Dados consolidados em ${moment(dataCliente).format("DD/MM/yyyy")}`} </Subtitle12>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        <ListArrowChart lock={lock} items={marketHeritageGrouped} />
                    </IonCol>
                </IonRow>
            </IonGrid>
        </TabsPage>
    )
}

export default Wallet;