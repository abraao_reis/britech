import { IonList, IonItem, IonGrid, IonRow, IonCol, IonImg } from "@ionic/react"
import { ChevronRight } from "assets/img"
import { Heading16, Subtitle12 } from "components/Typography"
import { IHeritage_Market } from "interface/market"
import LineChart from "./LineChart"
import { useSelector } from 'react-redux';
import { RootState } from 'store';
import { useHistory } from 'react-router';
import { useDispatch } from 'react-redux';
import { setFunds } from "store/reducers/funds"

interface IListArrowChartProps {
    items: IHeritage_Market[],
    lock:boolean,
}
export const ListArrowChart: React.FC<IListArrowChartProps> = (props) => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { marketProfitability } = useSelector((state:RootState)=>state.MarketReducer);
    const { marketHeritage } = useSelector((state:RootState)=>state.MarketReducer);
    const ListChartData = (mercado:string | number) => {
        const filteredMarketProfitability =  marketProfitability.filter( value => value.mercado === mercado);
        const data = filteredMarketProfitability.map((value:any)=>value.patrimonioFinalBrutoMoedaPortfolio);
        const date = filteredMarketProfitability.map((value:any)=>value.data);
        const dataChart = {
            labels:date.slice(date.length - 7 , date.length - 1),
            datasets: [{
                    backgroundColor: '#0BB783',
                    borderColor: '#0BB783',
                    data: data.slice(data.length - 7 , data.length - 1),
                    fill: true
            }]
        }
        return dataChart
    }
    return (
        <>
            <IonList>
                {props.items.map((item: IHeritage_Market, index: number) => (
                    <IonItem key={index}>
                        <IonGrid>
                            <IonRow className="on-justify-content-center ion-align-items-center">
                                <IonCol size="7">
                                    <Heading16 lock={props.lock}>{`${Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
                                                .format(item.valorLiquido)}`}
                                    </Heading16>
                                    <Subtitle12>{`${Math.floor(item.percentual * 100)}% em ${item.mercado ?? 'indefinido'} `}</Subtitle12>
                                </IonCol>
                                <IonCol size="4">
                                    <LineChart data={ListChartData(item.mercado)}  />
                                </IonCol>
                                <IonCol size="1" className="ion-align-self-center">
                                    <IonImg src={ChevronRight} 
                                        onClick={()=>{
                                            dispatch(setFunds({
                                                fundos:marketHeritage
                                                .filter( value => value.mercado === item.mercado),
                                                mercado:item.mercado,
                                            }));
                                            history.push('/home/fundos');
                                        }} 
                                    />
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonItem>
                ))}

            </IonList>
        </>
    )
}
