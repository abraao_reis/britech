import React from 'react';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);
export const options = {
    responsive: true,
    elements: {
        point:{
            radius: 0
        }
    },
    scales:{
        xAxis: {
            display:false
        },
        yAxis:{
            display:false
        }
    },
    plugins: {
        legend: {
            display: false
        },
        labels: {
            display: false
        }
    }
};
interface ILineChart {
    data: any
}
const LineChart: React.FC<ILineChart> = (props) => {

    return (
        <Line
            data={props.data}
            options={options}
        />
    )
}

export default LineChart