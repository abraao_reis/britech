import { IonCol, IonGrid, IonRow } from "@ionic/react";
import { Card, Select } from "components";
import TabsPage from "components/TabsPage";
import { Body14, Subtitle12 } from "components/Typography";
import { useDispatch, useSelector } from 'react-redux';
import React from "react";
import { RootState } from "store";
import EvolutionHeader from './EvolutionHeader';
import { EvolutionChart } from './EvolutionCharts';
import { LongPeriod } from "mock/select";
import { getProfitability, getProfitabilityMonth  , getProfitabilityYear , getProfitability12Months} from 'store/middleware';
import { heritageDataChart, profitabilityDataChart } from "./EvolutionCharts/DataChart";
import { ISelect } from 'interface/select';

enum ESegment {
    profitability = "Rentabilidade",
    heritage = "Patrimonio"
}
const Evolution: React.FC = () => {
    const dispatch = useDispatch();
    const [segment, setSegment] = React.useState('profitability');
    const [selected , setSelected] = React.useState('Este ano')
    const { profitability } = useSelector((state: RootState) => state.ProfitabilityReducer);
    const { lock } = useSelector((state:RootState)=> state.GenericReducer);
    
    React.useEffect(() => {
        dispatch(getProfitabilityYear())
    }, [dispatch]);

    const handleSelectEvolution = (select: ISelect) => {
        switch(select.value){
            case "ano":dispatch(getProfitabilityYear());
            break;
            case "inicio" : dispatch(getProfitability());
            break;
            case "mes" : dispatch(getProfitabilityMonth());
            break;
            case "12" : dispatch(getProfitability12Months());
            break;
        }
    }
    return (
        <TabsPage avatar={false} height="130px" subtitle="Evolução">
            <EvolutionHeader 
                segmentValue={segment}
                handleChangeSegment={(e)=>{setSegment(e)}}
            />
            <IonGrid>
                <IonRow>
                    <IonCol>
                        <EvolutionChart
                            data={ segment === 'profitability' ?
                                    profitabilityDataChart(profitability.labels , profitability.carteira , profitability.indice)
                                :
                                    heritageDataChart(profitability.labels, profitability.pl)
                            }
                        />
                    </IonCol>
                </IonRow>
                <IonRow className="ion-justify-content-between">
                    <IonCol size="6">
                        <Body14>
                            { 
                                ESegment[segment] 
                            }
                        </Body14>
                    </IonCol>
                    <IonCol size="4">
                        <Select 
                            title={selected}
                            items={LongPeriod}
                            onChangeSelect={(v) => { 
                                    handleSelectEvolution(v);
                                    setSelected(v.title); 
                                }
                            } 
                            menuTitle="Selecione um período"                    
                        />
                    </IonCol>
                </IonRow>
                <IonRow>
                    { segment === 'profitability' ? (
                        <>
                            <IonCol size="7">
                                <Card color="backgroundTertiary">
                                    <Body14>Carteira</Body14>
                                    <Subtitle12 lock={lock}>{`${Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
                            .format(Number(profitability.carteira[profitability.carteira.length - 1]))} | ${profitability.pcIndex[profitability.pcIndex.length - 1].toFixed(2)}% do CDI`}</Subtitle12>
                                </Card> 
                            </IonCol>
                            <IonCol size="5">
                                <Card color="backgroundTertiary">
                                    <Body14 color="success">CDI</Body14>
                                    <Subtitle12>{`${profitability.indice[profitability.indice.length - 1]}%`}</Subtitle12>
                                </Card>
                            </IonCol>
                        </>
                        ):(
                            <IonCol  size="12">
                                <Card color="backgroundTertiary">
                                    <Body14>Carteira</Body14>
                                    <Subtitle12 lock={lock}>{`${Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
                            .format(Number(profitability?.pl[profitability?.pl.length - 1]))} | ${profitability?.pcIndex[profitability?.pcIndex.length - 1].toFixed(2)}% do CDI`}</Subtitle12>
                                </Card>
                            </IonCol>              
                        )
                    }
                </IonRow>
            </IonGrid>
        </TabsPage>
    )
}

export default Evolution;