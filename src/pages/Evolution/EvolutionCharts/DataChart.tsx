export const profitabilityDataChart = (labels:string[] | number [] , carteira:number[] , indice:number[]) => {
    const data = {
        labels,
        datasets: [
            {
                data:carteira,
                borderColor: '#0BB783',
                backgroundColor: '#0BB783',
                fill:true
            },
            {
                data:indice,
                borderColor: '#002B3E',
                backgroundColor: '#002B3E',
                fill:true
            },
        ],
    };
    const options = {
        responsive: true,
        elements: {
            point:{
                radius: 0
            }
        },
        scale:{
            ticks: {
                display: false,
                maxTicksLimit: 0
            },
        },
        scales: { 
            yAxis: {
                type:"linear",
                title:{
                    display:true,
                    text:"valores em %",
                },
            },
        },
        plugins: {
            legend: {
                display: false
            },
            labels: {
                display: false
            }
        }
    };

    return {data, options}
};
export const heritageDataChart =  (labels:string[] | number[] , pl:number[]) => {
    const data = {
        labels:labels,
        datasets: [
            {
                data: pl,
                borderColor: "#002B3E",
                backgroundColor: '#002B3E',
                fill:true
            }
        ],
    };
    const options = {
        responsive: true,
        elements: {
            point:{
                radius: 0
            }
        },
        scale: { 
            ticks: {
                display: false,
                maxTicksLimit: 0
            }
        },
        plugins: {
            legend: {
                display: false
            },
            labels: {
                display: false
            }
        }
    };
    
    return {data, options}
}