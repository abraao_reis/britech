import React from 'react';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);
interface IEvolutionChart {
    data:any
}
export const EvolutionChart: React.FC<IEvolutionChart> = (props) => {
    return(
        <Line options={props.data.options} data={props.data.data} />
    )
}