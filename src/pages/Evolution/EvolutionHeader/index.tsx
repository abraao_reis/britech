import { IonLabel, IonSegment, IonSegmentButton } from "@ionic/react";
import { Card } from "components";
import './styles.css';
interface IEvolutionProps {
    segmentValue: string | undefined;
    handleChangeSegment: (segment:any) => void
}

const EvolutionHeader: React.FC<IEvolutionProps> = (props) => {
    return (
        <Card className="segment">
            <IonSegment onIonChange={(e)=>props.handleChangeSegment(e.detail.value)} value={props.segmentValue}>
                <IonSegmentButton value="profitability">
                    <IonLabel>Rentabilidade</IonLabel>
                </IonSegmentButton>
                <IonSegmentButton value="heritage">
                    <IonLabel>Patrimonio</IonLabel>
                </IonSegmentButton>
            </IonSegment>
        </Card>

    )
}

export default EvolutionHeader;