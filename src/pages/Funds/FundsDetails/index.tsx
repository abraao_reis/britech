import { IonGrid, IonRow, IonCol } from "@ionic/react";
import { Card } from "components";
import { Heading16, Subtitle12, Body16, Body14 } from "components/Typography";
import { IFundDetail, IRFDetail, IBMFDetail, IStockExchangeDetail } from "interface/funds";
import { currencyPtBR , dateFormatPtBR } from "utils/miscelaneous";


interface IFundsDetailsProps{
    FundDetails:IFundDetail[],
    lock:boolean
}
interface IBMFDetailsProps{
    BMFDetails:IBMFDetail[],
    lock:boolean
}
interface IRFDetailsProps{
    RFDetails:IRFDetail[],
    lock:boolean
}
interface IStockExchangeDetailsProps{
    StockExchangeDetails:IStockExchangeDetail[],
    lock:boolean
}
const FundsDetails:React.FC<IFundsDetailsProps> = (props) => {
    return(
        <>
        <IonGrid>
            <IonRow>
                <IonCol>
                    <Heading16 >{props.FundDetails[0]?.nome ?? '-' }</Heading16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Gestor</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{props.FundDetails[0]?.gestor ?? '-'}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Quantidade das cotas</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{props.FundDetails[0]?.quantidade.toLocaleString('pt-BR')}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Valor da cota</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{props.FundDetails[0]?.cotaDia}</Body16>
                </IonCol>
                <IonCol>
                    <Subtitle12 color="gray05">Valor da aplicação</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{currencyPtBR(props.FundDetails[0]?.custoMedio) ?? '-'}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Valor bruto</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{currencyPtBR(props.FundDetails[0]?.valorBruto) ?? '-'}</Body16>
                </IonCol>
                <IonCol>
                    <Subtitle12 color="gray05">% do PL</Subtitle12>
                    <Body16 lock={props.lock} color="primary">23,22%</Body16>
                </IonCol>
            </IonRow>
            <IonRow className="ion-justify-content-around">
                    <IonCol size="6">
                        <Card color="backgroundTertiary">
                            <Body14 color="primary">7%</Body14>
                            <Subtitle12 color="primary">Rentabilidade Mês</Subtitle12>
                        </Card>
                    </IonCol>
                    <IonCol size="6">
                        <Card color="backgroundTertiary">
                            <Body14 color="primary">7%</Body14>
                            <Subtitle12 color="primary">Rentabilidade Ano</Subtitle12>
                        </Card>
                    </IonCol>
            </IonRow>
        </IonGrid>
    </>
    )
}
const BMFDetails:React.FC<IBMFDetailsProps> = (props) => {
    return(
        <>
        <IonGrid>
            <IonRow>
                <IonCol>
                    <Body16 color="primary">{props.BMFDetails[0]?.emissor}</Body16>
                    <Heading16 >{props.BMFDetails[0]?.descricao}</Heading16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Série do Ativo</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{props.BMFDetails[0]?.serie ?? '-'}</Body16>
                </IonCol>
                <IonCol>
                    <Subtitle12 color="gray05">Vencimento</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{dateFormatPtBR(props.BMFDetails[0]?.dataVencimento) ?? '-'}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Pu Vencimento</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{props.BMFDetails[0]?.puVencimento}</Body16>
                </IonCol>
                <IonCol>
                    <Subtitle12 color="gray05">Quantidade</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{props.BMFDetails[0]?.quantidade.toLocaleString('pt-BR')}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">PU mercado</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{currencyPtBR(props.BMFDetails[0]?.precoAquisicao) ?? '-'}</Body16>
                </IonCol>
                <IonCol>
                    <Subtitle12 color="gray05">Valor mercado</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{currencyPtBR(props.BMFDetails[0]?.precoAtual) ?? '-'}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Ajuste diario</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{props.BMFDetails[0]?.ajusteDiario ?? '-'}</Body16>
                </IonCol>
                <IonCol>
                    <Subtitle12 color="gray05">% do PL</Subtitle12>
                    <Body16 lock={props.lock} color="primary">23,22%</Body16>
                </IonCol>
            </IonRow>
            <IonRow className="ion-justify-content-around">
                    <IonCol size="6">
                        <Card color="backgroundTertiary">
                            <Body14 color="primary">7%</Body14>
                            <Subtitle12 color="primary">Rentabilidade Mês</Subtitle12>
                        </Card>
                    </IonCol>
                    <IonCol size="6">
                        <Card color="backgroundTertiary">
                            <Body14 color="primary">7%</Body14>
                            <Subtitle12 color="primary">Rentabilidade Ano</Subtitle12>
                        </Card>
                    </IonCol>
            </IonRow>
        </IonGrid>
    </>
    )
}
const RFDetails:React.FC<IRFDetailsProps> = (props) => {
    return(
        <>
        <IonGrid>
            <IonRow>
                <IonCol>
                    <Body16 color="primary">{props.RFDetails[0]?.descricao}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Emissor</Subtitle12>
                    <Body16 color="primary">{props.RFDetails[0]?.emissor ?? '-'}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Data da compra</Subtitle12>
                    <Body16 color="primary">{dateFormatPtBR(props.RFDetails[0]?.dataCompra)}</Body16>
                </IonCol>
                <IonCol>
                    <Subtitle12 color="gray05">Data da Emissão</Subtitle12>
                    <Body16 color="primary">{dateFormatPtBR(props.RFDetails[0]?.dataEmissao)}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Data do vencimento</Subtitle12>
                    <Body16 color="primary">{dateFormatPtBR(props.RFDetails[0]?.dataVencimento)}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Taxa</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{Number(props.RFDetails[0]?.taxa).toFixed(2) ?? '-'}</Body16>
                </IonCol>
                <IonCol>
                    <Subtitle12 color="gray05">Índice</Subtitle12>
                    <Body16 color="primary">{props.RFDetails[0]?.indice ?? '-'}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Quantidade</Subtitle12>
                    <Body16 color="primary">{props.RFDetails[0]?.quantidade.toLocaleString('pt-BR') ?? '-'}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Valor Aplicação</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{currencyPtBR(props.RFDetails[0]?.valorAplicacao) ?? '-'}</Body16>
                </IonCol>
                <IonCol>
                    <Subtitle12 color="gray05">Pu mercado</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{currencyPtBR(props.RFDetails[0]?.precoAquisicao) ?? '-'}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Valor bruto</Subtitle12>
                    <Body16 color="primary">{currencyPtBR(props.RFDetails[0]?.valorBruto) ?? '-'}</Body16>
                </IonCol>
                <IonCol>
                    <Subtitle12 color="gray05">% do PL</Subtitle12>
                    <Body16 lock={props.lock} color="primary">23,22%</Body16>
                </IonCol>
            </IonRow>
            <IonRow className="ion-justify-content-around">
                    <IonCol size="6">
                        <Card color="backgroundTertiary">
                            <Body14 color="primary">7%</Body14>
                            <Subtitle12 color="primary">Rentabilidade Mês</Subtitle12>
                        </Card>
                    </IonCol>
                    <IonCol size="6">
                        <Card color="backgroundTertiary">
                            <Body14 color="primary">7%</Body14>
                            <Subtitle12 color="primary">Rentabilidade Ano</Subtitle12>
                        </Card>
                    </IonCol>
            </IonRow>
        </IonGrid>
    </>
    )
}
const StockExchangeDetails:React.FC<IStockExchangeDetailsProps> = (props) => {
    return(
        <>
        <IonGrid>
            <IonRow>
                <IonCol>
                    <Body16 color="primary">{props.StockExchangeDetails[0]?.emissor}</Body16>
                    <Heading16 >{props.StockExchangeDetails[0]?.descricao}</Heading16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Quantidade disponível</Subtitle12>
                    <Body16 color="primary">{props.StockExchangeDetails[0]?.quantidadeDisponivel?.toLocaleString('pt-BR') ?? '-'}</Body16>
                </IonCol>
                <IonCol>
                    <Subtitle12 color="gray05">Quantidade bloqueada</Subtitle12>
                    <Body16 color="primary">{props.StockExchangeDetails[0]?.quantidadeBloqueada?.toLocaleString('pt-BR') ?? '-'}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Quantidade total</Subtitle12>
                    <Body16 color="primary">{props.StockExchangeDetails[0]?.quantidade?.toLocaleString('pt-BR')}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">PU custo</Subtitle12>
                    <Body16 color="primary">{props.StockExchangeDetails[0]?.puVencimento}</Body16>
                </IonCol>
                <IonCol>
                    <Subtitle12 color="gray05">PU mercado</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{currencyPtBR(props.StockExchangeDetails[0]?.precoAtual) ?? '-'}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Valor mercado</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{currencyPtBR(props.StockExchangeDetails[0]?.valorBruto) ?? '-'}</Body16>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Subtitle12 color="gray05">Resultado Financeiro Acumulado na data base</Subtitle12>
                    <Body16 lock={props.lock} color="primary">{currencyPtBR(props.StockExchangeDetails[0]?.resultadoFinanceiroAcumulado) ?? '-'}</Body16>
                </IonCol>
                <IonCol>
                    <Subtitle12 color="gray05">% do PL</Subtitle12>
                    <Body16 color="primary">23,22%</Body16>
                </IonCol>
            </IonRow>
            <IonRow className="ion-justify-content-around">
                    <IonCol size="6">
                        <Card color="backgroundTertiary">
                            <Body14 color="primary">7%</Body14>
                            <Subtitle12 color="primary">Rentabilidade Mês</Subtitle12>
                        </Card>
                    </IonCol>
                    <IonCol size="6">
                        <Card color="backgroundTertiary">
                            <Body14 color="primary">7%</Body14>
                            <Subtitle12 color="primary">Rentabilidade Ano</Subtitle12>
                        </Card>
                    </IonCol>
            </IonRow>
        </IonGrid>
    </>
    )
}




export const ModalityFundDetails = {
    StockExchangeDetails,
    RFDetails,
    BMFDetails,
    FundsDetails
};