import { List, Modal, TabsPage } from "components"
import { IonCol, IonGrid, IonRow } from '@ionic/react';
import { RootState } from 'store';
import { useDispatch, useSelector } from "react-redux";
import { getFundsRFDetails, getFundsDetail, getFundsStockExchange, getFundsBMFDetails } from 'store/middleware/funds';
import { ModalityFundDetails } from "./FundsDetails";
import React from "react";
// import FundsDetails from "./FundsDetails";
import { getFundsTotal } from '../../store/middleware/funds/index';
import FundsHeader from './FundsHeader/index';


const Funds:React.FC = () => {
    const dispatch = useDispatch();
    const { funds , fund , total  } = useSelector((state:RootState)=>state.FundsReducer);
    const [modal,setModal] = React.useState(false);
    const [modalityFunds,setModalityFunds] = React.useState('Fundos');
    const { lock } = useSelector((state:RootState)=> state.GenericReducer);

    React.useEffect(()=>{
        dispatch(getFundsTotal(funds.mercado))
    },[dispatch, funds.mercado])

    const handleFundsDetails = (funds:string , cdAtivo: number | string | undefined) => {
        switch (funds) {
            case 'Renda Fixa':
                dispatch(getFundsRFDetails(cdAtivo))
            break;
            case 'Bolsa':
                dispatch(getFundsStockExchange(cdAtivo))
            break;
            case 'BMF':
                dispatch(getFundsBMFDetails(cdAtivo))
            break;
            case 'Fundos':
                dispatch(getFundsDetail(cdAtivo))
            break;
        }
    }

    return(
        <TabsPage avatar={false} subtitle={funds.mercado} component={<FundsHeader lock={lock} fundsTotal={total}/>}>
                <IonGrid>
                    <IonRow>
                        <IonCol>
                        <List.ListArrow
                            items={
                                funds.fundos
                                .map((value) => 
                                    ({
                                        title: Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
                                        .format(Number(value.valorLiquido)),
                                        subtitle:value.descricao,
                                        value:value.cdAtivo
                                    })
                                )
                            }
                            onClick={(item)=>{
                                handleFundsDetails(funds.mercado , item.value )
                                setModal(true)
                                setModalityFunds(funds.mercado)
                            }}
                        />
                        </IonCol>
                    </IonRow>
                </IonGrid>
            <Modal
                isOpen={modal}
                breakpoints={[0.4, 0.8, 1]}
                initialBreakpoint={0.8}
                onClick={() => { setModal(false) }}
                backdropDismiss={false}
                showBackdrop={true}
            >
                {{
                    'Fundos':<ModalityFundDetails.FundsDetails lock={lock} FundDetails={fund} />,
                    'BMF':<ModalityFundDetails.BMFDetails lock={lock} BMFDetails={fund} />,
                    'Renda Fixa':<ModalityFundDetails.RFDetails  lock={lock} RFDetails={fund} />,
                    'Bolsa':<ModalityFundDetails.StockExchangeDetails  lock={lock} StockExchangeDetails={fund} />
                }[modalityFunds]}
            </Modal>
        </TabsPage>
    )
}

export default Funds