import { IonGrid, IonRow, IonCol } from "@ionic/react";
import { Heading24 } from 'components/Typography';
import { IFundsTotal } from "interface/funds";
import { Subtitle12 } from '../../../components/Typography/index';

interface IFundsHeaderProps{
    fundsTotal:IFundsTotal,
    lock:boolean,
}
const FundsHeader:React.FC<IFundsHeaderProps> = (props) => {
    return(
        <IonGrid>
            <IonRow>
                <IonCol>
                    <Heading24 lock={props.lock}>{
                            Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
                            .format(props.fundsTotal.valorBruto)
                        }
                    </Heading24>
                    <Subtitle12>
                        Total atual bruto
                    </Subtitle12>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol style={{borderTop:'1px solid #ccc'}} ></IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <Heading24 lock={props.lock}>
                        {
                            Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
                            .format(props.fundsTotal.valorLiquido)
                        }
                    </Heading24>
                    <Subtitle12>
                        Total atual liquido
                    </Subtitle12>
                </IonCol>
            </IonRow>
        </IonGrid>
    )
}

export default FundsHeader;
