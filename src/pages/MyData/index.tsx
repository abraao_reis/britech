import { IonGrid, IonRow } from "@ionic/react"
import { Header, Input } from "components"
import React from "react"
import { useSelector, useDispatch } from "react-redux"
import { RootState } from "store"
import { getUserData } from "store/middleware"

const MyDataPage: React.FC = () => {
    const { userData } = useSelector((state: RootState) => state.UserDataReducer)
    const dispatch = useDispatch()
    React.useEffect(() => {
        dispatch(getUserData());
    }, [dispatch])
    return (
        <Header title="Meus Dados" fill={true}>
            <IonGrid className="ion-padding">
                <IonRow>
                    <Input type='string' disabled value={userData.nome} label="Nome" />
                </IonRow>
                <IonRow>
                    <Input type='string' disabled value={userData.cpfcnpj} label="Documento" />
                </IonRow>
                <IonRow>
                    <Input type='string' disabled value={userData.banco} label="Banco" />
                </IonRow>
                <IonRow>
                    <Input type='string' disabled value={userData.agencia} label="Agência" />
                </IonRow>
                <IonRow>
                    <Input type='string' disabled value={userData.numero} label="Número" />
                </IonRow>
                <IonRow>
                    <Input type='string' disabled value={userData.digitoConta} label="Dígito da Conta" />
                </IonRow>
            </IonGrid>
        </Header>
    )
}

export default MyDataPage