import { IonGrid, IonPage, IonCol, IonRow, IonContent, IonImg } from '@ionic/react';
import { CardSelector, Container, Header, BackButton } from 'components';
import { Body18, Subtitle12 } from 'components/Typography';
import './styles.css';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from 'store';
import { WALLETS } from 'interface/login';
import { setWallet } from 'store/reducers/wallet';
import { ChevronLeft } from 'assets/img';

const AccountSelector: React.FC = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { user } = useSelector((state: RootState) => state.UserReducer);
    return (
        <IonPage>
            <IonContent scrollEvents={true}>
                <Container>
                    <IonGrid>
                        <IonRow>
                            <IonCol size="2">
                                <IonImg src={ChevronLeft} onClick={()=>history.goBack()}/>
                            </IonCol>
                        </IonRow>
                        <IonRow class="ion-align-items-center">
                            <IonCol>
                                <IonCol>
                                    <Body18>
                                        Selecione uma conta para visualizar
                                    </Body18>
                                    <Subtitle12>
                                        Pode ter um texto explicativo aqui (opcional)
                                    </Subtitle12>
                                    <div className='accountContent'>
                                        {user.carteiras.map((wallet: WALLETS, index: number) => (
                                            <CardSelector
                                                key={index}
                                                title={wallet.nome}
                                                subtitle={wallet.nome}
                                                onClick={() => {
                                                    dispatch(
                                                        setWallet({ wallet: wallet })
                                                    )
                                                    history.push('/home');
                                                }}
                                            />
                                        ))
                                        }
                                    </div>
                                </IonCol>
                            </IonCol>
                        </IonRow>
                    </IonGrid>
                </Container>
            </IonContent>
        </IonPage>
    )
}

export default AccountSelector;