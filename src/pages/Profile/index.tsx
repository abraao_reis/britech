import { IonCol, IonGrid, IonImg, IonRow , IonContent } from "@ionic/react";
import { Britech, MenuClose, MyData, Shield, Change, Logout } from "assets/img";
import { Body16, Heading24 } from "components/Typography";
import './styles.css';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'store';
import { useHistory } from 'react-router';
import { setLogout } from "store/reducers/user";

const Profile: React.FC = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { user } = useSelector((state:RootState)=>state.UserReducer)
    return (
        <IonContent>
            <IonGrid>
                <IonRow className="">
                    <IonCol className="ion-padding">
                        <IonRow className="ion-justify-content-between ion-align-items-baseline">
                            <IonCol size="2" onClick={()=>{history.goBack()}}><IonImg src={MenuClose} /> </IonCol>
                            <IonCol size="2"><IonImg src={Britech} /></IonCol>
                        </IonRow>
                        <IonRow className="ion-margin-top">
                            <IonCol><Heading24>{user.nome}</Heading24></IonCol>
                        </IonRow>
                        <IonRow className="ion-align-items-center ion-padding-bottom ion-padding-top borderBottom">
                            <IonCol size="1"><IonImg src={Change} /></IonCol>
                            <IonCol size="7" onClick={()=>{ history.push('/accountSelector') }}>
                                <Body16 color="tertiary">Mudar Carteira</Body16>
                            </IonCol>
                        </IonRow>
                        <IonRow className="ion-align-items-center ion-margin-top ion-padding-top">
                            <IonCol size="1"><IonImg src={MyData} /></IonCol>
                            <IonCol size="7" onClick={()=>{ history.push('/home/meusdados') }}>
                                <Body16>Meus Dados</Body16>
                            </IonCol>
                        </IonRow>
                        <IonRow className="ion-align-items-center">
                            <IonCol size="1"><IonImg src={Shield} /></IonCol>
                            <IonCol size="7"><Body16>Privacidade e Segurança</Body16></IonCol>
                        </IonRow>
                    </IonCol>
                </IonRow>
                <IonRow style={{marginTop:'45vh'}} onClick={()=>{
                    dispatch(setLogout);
                    history.push('/')
                }}>
                    <IonCol>
                        <IonRow className="ion-align-items-center">
                            <IonCol size="1"><IonImg src={Logout} /></IonCol>
                            <IonCol size="7"><Body16>Sair</Body16></IonCol>
                        </IonRow>
                    </IonCol>
                </IonRow>
            </IonGrid>
        </IonContent>
    )
}
export default Profile;