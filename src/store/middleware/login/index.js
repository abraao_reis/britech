import { setUser } from "store/reducers/user";
import { setAuth } from "store/reducers/login";
import { setLoading } from "store/reducers/generic";
import { AuthService } from 'service';

export const postLogin = (user) => {
    return async (dispatch) => {
        dispatch(setLoading({ loading: 'true', message: 'Realizando login...' }));
        sessionStorage.removeItem('token');
        try {
            const res = await AuthService.Login(user);
            dispatch(setAuth(true))
            dispatch(setUser(res.data))
            dispatch(setLoading({ loading: false }))
        } catch (err) {
            dispatch(setLoading({ loading: false }));
            dispatch(setAuth(false))
            console.log(err)
        }
    };
};