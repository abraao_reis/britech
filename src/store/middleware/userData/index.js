import { UserDataService } from 'service'
import { setContato, setTransferencia, setDocument, setUserData } from 'store/reducers/userData';
export const getTransferencia = () => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const res = await UserDataService.TransferData(idCarteira);
            dispatch(setTransferencia({ transferencia: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};
export const getContato = () => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const res = await UserDataService.ContactData(idCarteira);
            dispatch(setContato({ contato: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};
export const getDocument = () => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const res = await UserDataService.DocumentData(idCarteira);
            dispatch(setDocument({ document: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};
export const getUserData = () => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const res = await UserDataService.UserData(idCarteira);
            dispatch(setUserData({ userdata: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};