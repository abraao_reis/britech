import { WalletService } from 'service';
import { DateStringConverter } from 'utils/miscelaneous';
import { setPatrimonio, setRentabilidade, setPatrimonioAgrupado } from 'store/reducers/market';
export const getMercadoPatrimonioCarteira = () => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const { dataCliente } = getState().UserReducer.user;
            const res = await WalletService.Market_Heritage(idCarteira, DateStringConverter(dataCliente));
            dispatch(setPatrimonio({ market: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};
export const getMercadoRentabilidadeCarteira = () => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const { dataCliente } = getState().UserReducer.user;
            const res = await WalletService.Market_ConsultationProfitability(idCarteira, DateStringConverter(dataCliente));
            dispatch(setRentabilidade({ market: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};

export const getMarketHeritageGrouped = () => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const { dataCliente } = getState().UserReducer.user;
            const res = await WalletService.Market_Heritage_Grouped(idCarteira, DateStringConverter(dataCliente));
            dispatch(setPatrimonioAgrupado({ market: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};

