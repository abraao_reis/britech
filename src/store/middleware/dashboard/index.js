import { DashboardService } from 'service';
import { DateStringConverter } from 'utils/miscelaneous';
import { setPatrimonio, setMercado } from 'store/reducers/dashboard'
import { setLoading } from 'store/reducers/generic';
export const getCarteiraPatrimonio = () => {
    return async (dispatch, getState) => {
        try {
            dispatch(setLoading(true))
            const { idCarteira } = getState().WalletReducer.wallet;
            const { dataCliente } = getState().UserReducer.user;
            const res = await DashboardService.Wallet_Heritage(idCarteira, DateStringConverter(dataCliente))
            dispatch(setPatrimonio({ dashboard: res.data[0] }));
            dispatch(setLoading(false))
        } catch (error) {
            dispatch(setLoading(false))
            console.log(error)
        }
    }
}
export const getCarteiraMercado = () => {
    return async (dispatch, getState) => {
        try {
            dispatch(setLoading(true))
            const { idCarteira } = getState().WalletReducer.wallet;
            const { dataCliente } = getState().UserReducer.user;
            const res = await DashboardService.Wallet_Market(idCarteira, DateStringConverter(dataCliente))
            dispatch(setMercado({ dashboard: res.data }));
            dispatch(setLoading(false))

        } catch (error) {
            dispatch(setLoading(false))
            console.log(error)
        }
    }
}