import { StatementService } from 'service';
import { setStatement } from 'store/reducers/statement';
import moment from 'moment';
export const getStatementIn = (period) => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const endDate = moment().format('MM-DD-YYYY');
            const startDate = moment().subtract(period, 'days').calendar();
            const res = await StatementService.Statement_In(idCarteira, startDate.replaceAll("/", "-"), endDate);
            dispatch(setStatement({ statement: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};
export const getStatementOut = (period) => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const endDate = moment().format('MM-DD-YYYY');
            const startDate = moment().subtract(period, 'days').calendar();
            const res = await StatementService.Statement_Out(idCarteira, startDate.replaceAll("/", "-"), endDate);
            dispatch(setStatement({ statement: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};
export const getStatementFuture = (period) => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const endDate = moment().format('MM-DD-YYYY');
            const startDate = moment().subtract(period, 'days').calendar();
            const res = await StatementService.Statement_Future(idCarteira, startDate.replaceAll("/", "-"), endDate);
            dispatch(setStatement({ statement: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};
export const getStatementAll = (period) => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const endDate = moment().format('MM-DD-YYYY');
            const startDate = moment().subtract(period, 'days').calendar();
            const res = await StatementService.Statement(idCarteira, startDate.replaceAll("/", "-"), endDate);
            dispatch(setStatement({ statement: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};