import { FundsServices } from "service";
import { DateStringConverter } from 'utils/miscelaneous';
import { setFundDetails, setFundsTotal } from 'store/reducers/funds';

export const getFundsDetail = (cdAtivo) => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const { dataCliente } = getState().UserReducer.user;
            const res = await FundsServices.Funds_Details(idCarteira, DateStringConverter(dataCliente), cdAtivo);
            dispatch(setFundDetails({ fund: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};
export const getFundsBMFDetails = (cdAtivo) => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const { dataCliente } = getState().UserReducer.user;
            const res = await FundsServices.Funds_BMFDetails(idCarteira, DateStringConverter(dataCliente), cdAtivo);
            dispatch(setFundDetails({ fund: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};
export const getFundsRFDetails = (cdAtivo) => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const { dataCliente } = getState().UserReducer.user;
            const res = await FundsServices.Funds_RFDetails(idCarteira, DateStringConverter(dataCliente), cdAtivo);
            dispatch(setFundDetails({ fund: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};
export const getFundsStockExchange = (cdAtivo) => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const { dataCliente } = getState().UserReducer.user;
            const res = await FundsServices.Funds_StockExchange(idCarteira, DateStringConverter(dataCliente), cdAtivo);
            dispatch(setFundDetails({ fund: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};

export const getFundsTotal = (cdAtivo) => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const { dataCliente } = getState().UserReducer.user;
            const res = await FundsServices.Funds_Total(idCarteira, DateStringConverter(dataCliente), cdAtivo);
            dispatch(setFundsTotal(res.data));
        } catch (err) {
            console.log(err)
        }
    };
};