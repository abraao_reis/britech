import { postLogin } from 'store/middleware/login';
import { getCarteiraPatrimonio, getCarteiraMercado } from 'store/middleware/dashboard'
import { getTransferencia, getContato, getDocument, getUserData } from 'store/middleware/userData';
import { getMercadoPatrimonioCarteira, getMercadoRentabilidadeCarteira, getMarketHeritageGrouped } from 'store/middleware/wallet';
import { getStatementFuture, getStatementIn, getStatementOut, getStatementAll } from 'store/middleware/statement';
import { getProfitability, getProfitability12Months, getProfitabilityMonth, getProfitabilityYear } from 'store/middleware/profitability';
import { getFundsDetail, getFundsBMFDetails, getFundsRFDetails, getFundsStockExchange, getFundsTotal } from 'store/middleware/funds';
export {
    postLogin,
    getCarteiraPatrimonio,
    getCarteiraMercado,
    getContato,
    getDocument,
    getTransferencia,
    getUserData,
    getMercadoPatrimonioCarteira,
    getMercadoRentabilidadeCarteira,
    getMarketHeritageGrouped,
    getStatementFuture,
    getStatementIn,
    getStatementOut,
    getStatementAll,
    getProfitability,
    getProfitability12Months,
    getProfitabilityMonth,
    getProfitabilityYear,
    getFundsDetail,
    getFundsBMFDetails,
    getFundsRFDetails,
    getFundsStockExchange,
    getFundsTotal
} 