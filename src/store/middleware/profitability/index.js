import { ProfitabilityService } from 'service';
import { DateStringConverter } from 'utils/miscelaneous';
import { setProfitability } from 'store/reducers/profitability';

export const getProfitability = () => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const { dataCliente } = getState().UserReducer.user;
            const res = await ProfitabilityService.Profitability_Wallet(idCarteira, DateStringConverter(dataCliente));
            dispatch(setProfitability({ profitability: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};
export const getProfitabilityMonth = () => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const { dataCliente } = getState().UserReducer.user;
            const res = await ProfitabilityService.Profitability_Wallet_Month(idCarteira, DateStringConverter(dataCliente));
            dispatch(setProfitability({ profitability: res.data.length > 0 ? res.data : null }));
            // gambiarra para apresentação
        } catch (err) {
            console.log(err)
        }
    };
};
export const getProfitability12Months = () => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const { dataCliente } = getState().UserReducer.user;
            const res = await ProfitabilityService.Profitability_Wallet_12Months(idCarteira, DateStringConverter(dataCliente));
            dispatch(setProfitability({ profitability: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};
export const getProfitabilityYear = () => {
    return async (dispatch, getState) => {
        try {
            const { idCarteira } = getState().WalletReducer.wallet;
            const { dataCliente } = getState().UserReducer.user;
            const res = await ProfitabilityService.Profitability_Wallet_Year(idCarteira, DateStringConverter(dataCliente));
            dispatch(setProfitability({ profitability: res.data }));
        } catch (err) {
            console.log(err)
        }
    };
};