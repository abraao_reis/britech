import { createSlice } from '@reduxjs/toolkit';
import moment from 'moment';

const initialState = {
    profitability: {
        labels: [1, 2, 3, 4, 5, 6],
        carteira: [10, 20, 30, 40, 50, 60],
        indice: [2, 4, 6, 8, 10, 12],
        pl: [3, 6, 9, 12, 15, 18],
        pcIndex: [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    }
}
const profitabilitySlice = createSlice({
    name: 'Profitability',
    initialState,
    reducers: {
        setProfitability: {
            reducer(state, action) {
                const carteira = action.payload.profitability.map((value) => value.carteira * 100);
                const indice = action.payload.profitability.map((value) => value.indice * 100);
                const labels = action.payload.profitability.map((value) => moment(value.data).format('MM-YYYY'));
                const pl = action.payload.profitability.map((value) => value.pl);
                const pcIndex = action.payload.profitability.map((value) => value.pcIndex);
                state.profitability = {
                    carteira,
                    indice,
                    labels,
                    pl,
                    pcIndex
                }
            },
            prepare({ profitability }) {
                return { payload: { profitability } }
            }
        },
    }
});

export const { setProfitability } = profitabilitySlice.actions;
export default profitabilitySlice.reducer;