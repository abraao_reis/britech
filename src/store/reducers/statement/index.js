import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    statement: [{
        dataVencimento: "13-10-2021",
        descricao: "Aplicação mensal",
        valor: 3200,
        saldo: 50000
    }]
};
const statementSlice = createSlice({
    name: 'Statement',
    initialState,
    reducers: {
        setStatement: {
            reducer(state, action) {
                state.statement = action.payload.statement
            },
            prepare({ statement }) {
                return { payload: { statement } }
            }
        },
    }
});

export const { setStatement } = statementSlice.actions;
export default statementSlice.reducer;