import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    transferencia: {
        banco: '',
        cc: '',
        tipoTransferencia: '',
        finalidade: ''

    },
    contato: {
        nome: '',
        telefone: '',
        email: '',
        endereco: ''

    },
    document: [{
        "id": 30,
        "guid": "3e286ce4-bd1f-4e60-9001-c530f08fa2f9",
        "idCliente": 10251,
        "nameFile": "Memorial",
        "fileData": "DQoxIC0gUXVlbSBmb2kgYSB2ZW5kZWRvcmEgDQoyIA0KICAgICAg",
        "typeFile": "1",
        "date": "2021-05-19T15:58:24.563",
        "extensao": "txt",
        "url": "http://localhost/framcapitalfiles//Files/10251/2022-04-12/be0de82e-766a-441f-b274-b41bd7b67a75.txt",
        "fileBytes": [0]
    },
    ],
    userData: {
        nome: "",
        cpfcnpj: "",
        banco: "",
        agencia: "",
        numero: "",
        digitoConta: ""
    }
}
const userDataSlice = createSlice({
    name: 'UserData',
    initialState,
    reducers: {
        setTransferencia: {
            reducer(state, action) {
                state.transferencia = action.payload.transferencia
            },
            prepare({ transferencia }) {
                return { payload: { transferencia } }
            }
        },
        setContato: {
            reducer(state, action) {
                state.contato = action.payload.contato
            },
            prepare({ contato }) {
                return { payload: { contato } }
            }
        },
        setDocument: {
            reducer(state, action) {
                state.document = action.payload.document
            },
            prepare({ document }) {
                return { payload: { document } }
            }
        },
        setUserData: {
            reducer(state, action) {
                state.userData = action.payload.userdata
            },
            prepare({ userdata }) {
                return { payload: { userdata } }
            }
        },
    }
});

export const { setTransferencia, setContato, setDocument, setUserData } = userDataSlice.actions;
export default userDataSlice.reducer;