import { createSlice } from '@reduxjs/toolkit';
const initialState = {
    isLoggedIn: false
}
const authSlice = createSlice({
    name: 'Auth',
    initialState,
    reducers: {
        setAuth: {
            reducer(state) {
                state.isLoggedIn = true
            },
            prepare(payload) {
                return { payload }
            }
        }
    }
});
export const { setAuth } = authSlice.actions;

export default authSlice.reducer;