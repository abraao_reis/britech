import { createSlice } from '@reduxjs/toolkit';
const initialState = {
    user: {
        carteiras: [],
        email: '',
        idUsuario: 1,
        login: '',
        nome: '',
        dataCliente: '22-10-2020'
    },
    token: '',
}
const userSlice = createSlice({
    name: 'User',
    initialState,
    reducers: {
        setUser: {
            reducer(state, action) {
                state.user = action.payload.user;
                localStorage.setItem('token', action.payload.token);
            },
            prepare({ user, token }) {
                return { payload: { user, token } };
            },
        },
        setLogout: {
            reducer(state) {
                state.user = null;
                localStorage.setItem('token', null);
                localStorage.setItem('root:persist', null);
            },
        }
    }
});
export const { setUser, setLogout } = userSlice.actions;

export default userSlice.reducer;