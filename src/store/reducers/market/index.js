import { createSlice } from '@reduxjs/toolkit';
const initialState = {
    marketHeritage: [
        {
            mercado: "Fundos",
            idEstrategia: "22",
            tipo: "Fundos",
            cdAtivo: "530530",
            descricao: "VERDE AM L B 70 ICATU PREV FIE FI MULT",
            percentual: 0.125458,
            posicao: 374107.23,
            valorLiquido: 374107.23
        }
    ],
    marketHeritageGrouped: [
        {
            mercado: "Fundos",
            idEstrategia: "22",
            tipo: "Fundos",
            cdAtivo: "530530",
            descricao: "VERDE AM L B 70 ICATU PREV FIE FI MULT",
            percentual: 0.125458,
            posicao: 374107.23,
            valorLiquido: 374107.23
        }
    ],
    marketProfitability: [{
        "idRentabilidade": 0,
        "data": "2018-10-03T00:00:00",
        "tipoAtivo": 0,
        "idCliente": 0,
        "patrimonioInicialMoedaPortfolio": 0.00,
        "valorFinanceiroEntradaMoedaPortfolio": 700000.00,
        "patrimonioFinalBrutoMoedaPortfolio": 700000.00,
        "rendimentoBrutoMoedaPortfolio": 0.00,
        "idEstrategia": 2,
        "estrategia": "Renda Fixa",
        "mercado": "Renda Fixa"

    }]
}
const marketSlice = createSlice({
    name: 'Market',
    initialState,
    reducers: {
        setPatrimonio: {
            reducer(state, action) {
                state.marketHeritage = action.payload.market;
            },
            prepare({ market }) {
                return { payload: { market } };
            },
        },
        setPatrimonioAgrupado: {
            reducer(state, action) {
                state.marketHeritageGrouped = action.payload.market;
            },
            prepare({ market }) {
                return { payload: { market } }
            }
        },
        setRentabilidade: {
            reducer(state, action) {
                state.marketProfitability = action.payload.market;
            },
            prepare({ market }) {
                return { payload: { market } };
            },
        },

    }
});
export const { setPatrimonio, setRentabilidade, setPatrimonioAgrupado } = marketSlice.actions;

export default marketSlice.reducer;