import { createSlice } from '@reduxjs/toolkit';
const initialState = {
    loading: false,
    message: 'Carregando',
    segment: 'profitability',
    lock: false,
}
const genericSlice = createSlice({
    name: 'Generic',
    initialState,
    reducers: {
        setLoading: {
            reducer(state, action) {
                state.loading = action.payload.loading;
                state.message = action.payload.message
            },
            prepare({ loading, message }) {
                return { payload: { loading, message } };
            },
        },
        setSegment: {
            reducer(state, action) {
                state.segment = action.payload.segment;
            },
            prepare({ segment }) {
                return { payload: { segment } };
            },
        },
        setLock: {
            reducer(state, action) {
                state.lock = action.payload.lock;
            },
            prepare(lock) {
                return { payload: { lock } };
            },
        }
    }
});
export const { setLoading, setSegment, setLock } = genericSlice.actions;

export default genericSlice.reducer;