import { combineReducers } from "redux";
import AuthReducer from 'store/reducers/login';
import GenericReducer from 'store/reducers/generic';
import WalletReducer from 'store/reducers/wallet';
import UserReducer from 'store/reducers/user';
import DashReducer from 'store/reducers/dashboard';
import UserDataReducer from 'store/reducers/userData';
import MarketReducer from 'store/reducers/market';
import StatementReducer from 'store/reducers/statement';
import ProfitabilityReducer from 'store/reducers/profitability';
import FundsReducer from 'store/reducers/funds';

export default combineReducers({
    AuthReducer,
    UserDataReducer,
    GenericReducer,
    WalletReducer,
    UserReducer,
    DashReducer,
    MarketReducer,
    StatementReducer,
    ProfitabilityReducer,
    FundsReducer
})