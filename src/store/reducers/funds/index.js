import { createSlice } from '@reduxjs/toolkit';
const initialState = {
    funds: {
        fundos: [{
            mercado: "Fundos",
            idEstrategia: "22",
            tipo: "Fundos",
            cdAtivo: "530530",
            descricao: "VERDE AM L B 70 ICATU PREV FIE FI MULT",
            percentual: 0.125458,
            posicao: 374107.23,
            valorLiquido: 374107.23
        }],
        mercado: 'fundos'
    },
    fund: [{
        "nome": "VERDE AM L B 70 ICATU PREV FIE FI MULT  ",
        "gestor": "Verde Asset Management SA",
        "codigoAtivo": "4559",
        "custoMedio": 6.139195656465,
        "cotaDia": 0.997507300000,
        "descricao": "CRI Saint Francis - 17G1674856",
        "dataEmissao": "07/17/2017 00:00:00",
        "dataVencimento": "10/07/2022 00:00:00",
        "quantidade": 2.000000000000,
        "precoAquisicao": 43292.190000,
        "precoAtual": 42300.897480064600,
        "valorBruto": 84601.79,
        "valorLiquido": 0,
        "emissor": "SAINT FRANCIS",
        "dataCompra": "11/27/2019 00:00:00",
        "taxa": "11.0000000000000000",
        "indice": "IGPM",
        "valorAplicacao": "86584.38000000000",
        "quantidadeBloqueada": 0,
        "quantidadeDisponivel": 0,
        "resultadoFinanceiroAcumulado": 0,
        "ajusteDiario": 0,
        "puVencimento": 0
    }],
    total: {
        quantidade: 0,
        custoMedio: 0,
        cotaDia: 0,
        valorBruto: 0,
        valorLiquido: 0
    }

}
const fundsSlice = createSlice({
    name: 'Funds',
    initialState,
    reducers: {
        setFunds: {
            reducer(state, actions) {
                state.funds = actions.payload;
            },
            prepare(payload) {
                return { payload }
            }
        },
        setFundDetails: {
            reducer(state, actions) {
                state.fund = actions.payload.fund;
            },
            prepare(payload) {
                return { payload }
            }
        },
        setFundsTotal: {
            reducer(state, actions) {
                state.total = actions.payload;
            },
            prepare(payload) {
                return { payload }
            }
        }
    }
});
export const { setFunds, setFundDetails, setFundsTotal } = fundsSlice.actions;

export default fundsSlice.reducer;