import { createSlice } from '@reduxjs/toolkit';
const initialState = {
    wallet: {
        idCarteira: "",
        nome: "",
    },
}
const walletSlice = createSlice({
    name: 'Wallet',
    initialState,
    reducers: {
        setWallet: {
            reducer(state, action) {
                state.wallet = action.payload.wallet;
            },
            prepare({ wallet }) {
                return { payload: { wallet } };
            },
        },
    }
});
export const { setWallet } = walletSlice.actions;

export default walletSlice.reducer;