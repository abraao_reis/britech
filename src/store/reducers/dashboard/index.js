import { createSlice } from '@reduxjs/toolkit';
import { DataGroupByElement, generationRandomColor, sumArray } from 'utils/miscelaneous'
const initialState = {
    patrimonio: {
        idCarteira: "",
        patrimonioTotal: "",
        patrimonioCarteira: "",
        contaCorrente: "",
        contaCorrenteCPR: ""
    },
    mercado: {
        labels: [],
        datasets: [
            {
                legends: ["# teste"],
                legendsColors: ["#ffffff"],
                data: [0],
                backgroundColor: [
                    '#fff',
                ],
                borderColor: [
                    '#fff',
                ],
                borderWidth: 1,
            },
        ],
    }
}
const dashSlice = createSlice({
    name: 'Dashboard',
    initialState,
    reducers: {
        setPatrimonio: {
            reducer(state, action) {
                state.patrimonio = action.payload.dashboard;
            },
            prepare({ dashboard }) {
                return { payload: { dashboard } };
            }
        },
        setMercado: {
            reducer(state, action) {
                const groupArray = DataGroupByElement(action.payload.dashboard);
                state.mercado = {
                    labels: [],
                    datasets: [{
                        legends: Object.keys(groupArray),
                        legendsColors: ["#002B3E", "#3DC5E7", "#4472C4", "#999999", "#5B9BD5", "#70AD47", "#264478", "#F4B183", "#FFD966", "#D1B2E8"],
                        backgroundColor: ["#002B3E", "#3DC5E7", "#4472C4", "#999999", "#5B9BD5", "#70AD47", "#264478", "#F4B183", "#FFD966", "#D1B2E8"],
                        data: sumArray(groupArray),
                        borderWidth: 1,
                    }]
                };
            },
            prepare({ dashboard }) {
                return { payload: { dashboard } };
            }
        }
    }
});
export const { setPatrimonio, setMercado } = dashSlice.actions;

export default dashSlice.reducer;