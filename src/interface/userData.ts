export interface IDocument {
    id: number,
    guid: string,
    idCliente: number,
    nameFile: string,
    fileData: string,
    typeFile: string,
    date: string,
    extensao: string,
    url:string,
    fileBytes: number[]
}

export interface IDocumentList {
    list: IDocument[]
}