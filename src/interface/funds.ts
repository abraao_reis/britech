export interface IFundDetail {
    idCarteira: string,
    nome: string,
    gestor: string,
    quantidade: number,
    custoMedio: number,
    cotaDia: number,
    valorBruto: number,
    valorLiquido: number
}
export interface IBMFDetail {
    codigoAtivo: string,
    descricao: string,
    dataEmissao: string,
    dataVencimento: string,
    quantidade: number,
    precoAquisicao: number,
    precoAtual: number,
    valorBruto: number,
    emissor: string,
    dataCompra: string,
    taxa: string,
    indice: string,
    valorAplicacao: string,
    quantidadeBloqueada: number,
    quantidadeDisponivel: number,
    resultadoFinanceiroAcumulado: number,
    ajusteDiario: number,
    puVencimento: number,
    serie?: string
}
export interface IRFDetail {
    codigoAtivo: string,
    descricao: string,
    dataEmissao: string,
    dataVencimento: string,
    quantidade: number,
    precoAquisicao: number,
    precoAtual: number,
    valorBruto: number,
    emissor: string,
    dataCompra: string,
    taxa: string,
    indice: string,
    valorAplicacao: string,
    quantidadeBloqueada: number,
    quantidadeDisponivel: number,
    resultadoFinanceiroAcumulado: number,
    ajusteDiario: number,
    puVencimento: number,
    serie?: string
}
export interface IStockExchangeDetail {
    codigoAtivo: string,
    descricao: string,
    dataEmissao: string,
    dataVencimento: string,
    quantidade: number,
    precoAquisicao: number,
    precoAtual: number,
    valorBruto: number,
    emissor: string,
    dataCompra: string,
    taxa: string,
    indice: string,
    valorAplicacao: string,
    quantidadeBloqueada: number,
    quantidadeDisponivel: number,
    resultadoFinanceiroAcumulado: number,
    ajusteDiario: number,
    puVencimento: number,
    serie?: string
}

export interface IFundsTotal {
    quantidade: number,
    custoMedio: number,
    cotaDia: number,
    valorBruto: number,
    valorLiquido: number
}