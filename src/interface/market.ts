export interface IHeritage_Market {
    mercado: string,
    idEstrategia: string | number,
    tipo: string,
    cdAtivo: string,
    descricao: string,
    percentual: number,
    posicao: number,
    valorLiquido: number,
} 