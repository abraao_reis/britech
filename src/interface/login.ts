interface WALLETS {
    idCarteira: number,
    nome: string
};
interface LOGIN {
    carteiras: WALLETS,
    email?: string,
    idUsuario?: number,
    login?: string,
    nome?: string,
}

export type {
    WALLETS,
    LOGIN
}