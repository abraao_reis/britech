
export interface IStatement {
    dataVencimento: string,
    descricao: string,
    valor: number,
    saldo: number
}
export interface IStatementList {
    items: IStatement[],
    lock:boolean,
}