import AlertCircle from 'assets/img/alert-circle.svg';
import Check from 'assets/img/check.svg';
import Eyesoff from 'assets/img/eyes-off.svg';
import Eyeson from 'assets/img/eyes-on.svg'
import ChevronRight from 'assets/img/chevron-right.svg';
import ChevronLeft from 'assets/img/chevron-left.svg';
import ChevronDown from 'assets/img/chevron-down.svg';
import ButtonRightNarrow from 'assets/img/button-right-narrow.svg'
import Home from 'assets/img/home.svg';
import Wallet from 'assets/img/wallet.svg';
import Chart from 'assets/img/chart.svg';
import Document from 'assets/img/document.svg';
import NotificationAlert from 'assets/img/notification-alert.svg';
import Notification from 'assets/img/notification.svg';
import Transferencia from 'assets/img/transferencia.svg';
import Call from 'assets/img/call.svg';
import Folder from 'assets/img/folder.svg';
import CloseModal from 'assets/img/close-modal.svg';
import WalletPurple from 'assets/img/wallet-purple.svg';
import ArrowUp from 'assets/img/arrow-up.svg';
import ArrowDown from 'assets/img/arrow-down.svg';
import MoneyCard from 'assets/img/money-card.svg';
import Download from 'assets/img/download.svg';
import Britech from 'assets/img/britech.svg';
import MenuClose from 'assets/img/menu-close.svg';
import MyData from 'assets/img/my-data.svg';
import Shield from 'assets/img/shield.svg';
import Change from 'assets/img/change.svg';
import Logout from 'assets/img/logout.svg';
export {
    AlertCircle,
    Check,
    Eyesoff,
    Eyeson,
    ChevronRight,
    ButtonRightNarrow,
    Home,
    Wallet,
    Chart,
    Document,
    NotificationAlert,
    Notification,
    Transferencia,
    Call,
    Folder,
    CloseModal,
    WalletPurple,
    ChevronLeft,
    ArrowUp,
    ArrowDown,
    MoneyCard,
    Download,
    Britech,
    MenuClose,
    MyData,
    Shield,
    Change,
    ChevronDown,
    Logout,
}