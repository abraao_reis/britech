

export const LongPeriod =     
    [
        {
            title:'Início',
            value:'inicio'
        },
        {
            title:'Mês',
            value:'mes'
        },
        {
            title:'12 meses',
            value:'12' 
        },
        {
            title:'Ano',
            value:'ano' 
        }
    ]

export const ShortPeriod = 
    [
        {
            title:'15 dias',
            value:'15'
        },
        {
            title:'30 dias',
            value:'30'
        },
        {
            title:'60 dias',
            value:'60' 
        },
        {
            title:'90 dias',
            value:'90' 
        }
    ]
