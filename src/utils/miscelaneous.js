import moment from 'moment';

export function DateStringConverter(s) {
    const y = s.substring(0, 4);
    const d = s.substring(4, 6);
    const m = s.substring(6, 8)
    return `${y}-${d}-${m}`;
}

export const DataGroupByElement = (array) => array.reduce((group, el) => {
    const { tipo } = el;
    group[tipo] = group[tipo] ?? [];
    group[tipo].push(el);
    return group;
}, {});

// graphics function

export const generationRandomColor = (array) => {
    const size = Object.values(array).length
    let colors = [];
    // n => number of colors
    for (let i = 0; i < size; i++) {
        colors[i] = (`#${Math.floor(Math.random() * 16777215).toString(16)}`);
    }
    return colors;
}

export const sumArray = (array) => {
    const dataArray = Object.values(array)
    let sum = []
    for (let i = 0; i < dataArray.length; i++) {
        for (let j = 0; j < dataArray[i].length; j++) {
            sum[i] = + dataArray[i][j].valorLiquido
        }
    }
    return sum
}
export const currencyPtBR = (value) => {
    return Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' })
        .format(value)
}
export const dateFormatPtBR = (date) => {
    return moment(date).format('DD-MM-yyyy');
}
export const LiteralMonths = [
    'Janeiro',
    'Fevereiro',
    'Março',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro'
]