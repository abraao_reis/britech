import axios from 'axios';
const config = {
    headers: {
        'Content-type': 'application/json',
    }
}
const HttpClient = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL,
    config
});

HttpClient.interceptors.response.use((response) => {
    return response;
}, (error) => {
    const config = error.config;
    const token = localStorage.getItem("token");
    if (error.response) {
        if (error.response.status === 401 && !config._retry) {
            config._retry = true;
            config.headers.Authorization = `Bearer ${token}`;
            return axios(config);
        }
        if (error.response.status === 403) {
            // Do something 
            return Promise.reject(error.response.data);
        }
    }
    return Promise.resolve({ error });
});
HttpClient.interceptors.request.use((request) => {
    const token = localStorage.getItem("token");
    request.headers.Authorization = `Bearer ${token}`;
    return request;
}, (error) => {
    return Promise.resolve({ error });
});

export default HttpClient;