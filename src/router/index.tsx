import { Redirect, Route } from 'react-router-dom';
import { IonReactHashRouter } from '@ionic/react-router';
import { IonRouterOutlet } from '@ionic/react';
import {
    Login,
    AccountSelector,
} from 'pages'
import TabsRoute from './tabs';
import { Loading } from 'components';
import { useSelector } from 'react-redux';
import { RootState } from 'store';
import React, { useEffect, useState } from 'react';

const AppRoutes: React.FC = () => {
    const { loading } = useSelector((state: RootState) => state.GenericReducer);
    const [loader, setLoader] = useState(loading)
    useEffect(() => {
        setLoader(loading)
    }, [loading])
    return (
        <IonReactHashRouter>
            {loader ? <Loading /> :
                <IonRouterOutlet>
                    <Route exact path="/login">
                        <Login />
                    </Route>
                    <Route exact path='/'>
                        <Redirect to='/login' />
                    </Route>
                    <Route path="/accountSelector">
                        <AccountSelector />
                    </Route>
                    <Route path="/home">
                        <TabsRoute />
                    </Route>
                </IonRouterOutlet>
            }
        </IonReactHashRouter>

    )
}

export default AppRoutes;