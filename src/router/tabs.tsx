import { IonContent, IonIcon, IonRouterOutlet, IonTabBar, IonTabButton, IonTabs, } from "@ionic/react";
import { Chart, Document, Home, Wallet } from "assets/img";
import { Redirect, Route } from 'react-router-dom';
import {
    Dashboard,
    WalletPage,
    EvolutionPage,
    StatementPage,
    DocumentPage,
    MyDataPage,
    ProfilePage,
    FundsPage
} from 'pages';
import './style.css'
import React from "react";
const TabsRoute: React.FC = () => {
    const [activeTab, setActiveTab] = React.useState("Home")
    return (
            <IonContent id="main" scrollEvents={true}>
                <IonTabs>
                    <IonRouterOutlet>
                        <Route exact path="/home" render={() => <Redirect to="/home/dashboard"/> }/>
                        <Route path="/home/dashboard" component={Dashboard}/>
                        <Route path="/home/carteira" component={WalletPage} />
                        <Route path="/home/evolucao" component={EvolutionPage} />
                        <Route path="/home/extrato" component={StatementPage} />
                        <Route path="/home/documentos" component={DocumentPage}/>
                        <Route path="/home/meusdados" component={MyDataPage}/>
                        <Route path="/home/perfil" component={ProfilePage}/>
                        <Route path="/home/fundos" component={FundsPage}/>
                    </IonRouterOutlet>
                    <IonTabBar onIonTabsWillChange={(e)=>{ setActiveTab(e.detail.tab) }} slot="bottom">
                        <IonTabButton tab="Home" href="/home/dashboard">
                            <IonIcon className={activeTab==="Home" ? "activatedTab" : "deactivated"} src={Home} size="small" />
                        </IonTabButton>
                        <IonTabButton tab="Wallet" href="/home/carteira">
                            <IonIcon className={activeTab==="Wallet" ? "activatedTab" : "deactivated"} src={Wallet} size="small" />
                        </IonTabButton>
                        <IonTabButton tab="Evolution" href="/home/evolucao" >
                            <IonIcon className={activeTab==="Evolution" ? "activatedTab" : "deactivated"} src={Chart} size="small" />
                        </IonTabButton>
                        <IonTabButton tab="Statement" href="/home/extrato">
                            <IonIcon className={activeTab==="Statement" ? "activatedTab" : "deactivated"} src={Document} size="small" />
                        </IonTabButton>
                    </IonTabBar>
                </IonTabs>
            </IonContent>
    )
}

export default TabsRoute;