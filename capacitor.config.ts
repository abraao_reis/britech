import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'br.britech.mobile',
  appName: 'britech',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
